package com.eyecu.ane.ios.userinput{
	
	import flash.events.Event;
	
	public class UserInputEvent extends Event{
		
		private static const _onDialogClose:String = "onDialogClose";
		private static const _bottomTextInputSubmit:String = "bottomTextInputSubmit";
		private static const _topTextInputSubmit:String = "topTextInputSubmit";
        private static const _topTextInputClosed:String = "topTextInputClosed";

        private static const _detailsViewClosed:String = "detailsViewClosed";
        private static const _detailsViewClosing:String = "detailsViewClosed";
        private static const _detailsItemSelected:String = "detailsItemSelected";
        private static const _detailsMultiItemSelected:String = "detailsMultiItemSelected";
        private static const _userItemSelected:String = "userItemSelected";

        private static const _userDataRequested:String = "userDataRequested";
        private static const _detailsDataRequested:String = "detailsDataRequested";
        private static const _inboxDataRequested:String = "inboxDataRequested";
        private static const _shareDataRequested:String = "shareDataRequested";

        private static const _actionSheetClosed:String = "actionSheetClosed";

        private static const _shareWithIcebreakerFriend:String = "shareWithIcebreakerFriend";
        private static const _shareWithFacebookFriend:String = "shareWithFacebookFriend";

        private static const _sharedQuestionDeleted:String = "sharedQuestionDeleted";
        private static const _topDataRequested:String = "topDataRequested";
        private static const _viewDidChange:String = "viewDidChange";


		public var didSubmit:Boolean;
		public var text:String;
        public var data:Object;

        public static function get DETAILS_MULTI_ITEM_SELECTED():String{
            return _detailsMultiItemSelected;
        }

        public static function get VIEW_DID_CHANGE():String{
            return _viewDidChange;
        }

        public static function get TOP_DATA_REQUESTED():String{
            return _topDataRequested;
        }

        public static function get SHARED_QUESTION_DELETED():String{
            return _sharedQuestionDeleted;
        }

        public static function get SHARE_CURRENT_QUESTION_WITH_FACEBOOK_FRIEND():String{
            return _shareWithFacebookFriend;
        }

        public static function get SHARE_CURRENT_QUESTION_WITH_ICEBREAKER_FRIEND():String{
            return _shareWithIcebreakerFriend;
        }

        public static function get SHARE_DATA_REQUESTED():String{
            return _shareDataRequested;
        }

        public static function get ACTION_SHEET_CLOSED():String{
            return _actionSheetClosed;
        }

        public static function get INBOX_DATA_REQUESTED():String{
            return _inboxDataRequested;
        }

        public static function get USER_ITEM_SELECTED():String{
            return _userItemSelected;
        }

        public static function get USER_DATA_REQUESTED():String{
            return _userDataRequested;
        }

        public static function get DETAILS_DATA_REQUESTED():String{
            return _detailsDataRequested;
        }

        public static function get DETAILS_ITEM_SELECTED():String{
            return _detailsItemSelected;
        }

        public static function get DETAILS_VIEW_CLOSING():String{
            return _detailsViewClosing;
        }


        public static function get DETAILS_VIEW_CLOSED():String{
            return _detailsViewClosed;
        }


        public static function get TOP_TEXT_INPUT_CLOSED():String{
            return _topTextInputClosed;
        }

		public static function get TOP_TEXT_INPUT_SUBMIT():String{
			return _topTextInputSubmit;
		}
		
		public static function get ON_DIALOG_CLOSE():String{
			return _onDialogClose;
		}
		
		public static function get BOTTOM_TEXT_INPUT_SUBMIT():String{
			return _bottomTextInputSubmit;
		}
		
		public function UserInputEvent(type:String):void{
			
			super(type, false, false);
		}
	}
	
}