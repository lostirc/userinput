package com.eyecu.ane.ios.userinput{
    import com.eyecu.ane.ios.userinput.UserInputEvent;

    import flash.events.EventDispatcher;
	import flash.external.ExtensionContext;
	import flash.events.StatusEvent;

	
	public class UserInput extends EventDispatcher{
		private var _extContext:ExtensionContext;
		private static var _instance:UserInput;
		
		private var _bottomTextInputCreated:Boolean;
		private var _topTextInputCreated:Boolean;
		
		public function UserInput(enf:SingletonEnforcer){
			_extContext = ExtensionContext.createExtensionContext("com.eyecu.ane.ios.userinput.UserInput", "" );
			_extContext.addEventListener( StatusEvent.STATUS, onStatus );
		}
		
		public static function get instance():UserInput {
			if ( !_instance ){
				_instance = new UserInput( new SingletonEnforcer() );
				_instance.init();
			}
			return _instance;
		}
		
		private function onStatus(e:StatusEvent):void{
			trace("on status: " + e.code + " " + e.level);
			
			var response:Object = JSON.parse(e.level);
			
			if(e.code == "windowClosedEvent"){
				dispatchDialogCloseEvent((response["submitted"] == "true"), response["text"]);
			} else if(e.code == "bottomTextInputSubmit"){
				dispatchBottomTextSubmittedEvent(unescape(response["text"]));
			} else if(e.code == "topTextInputSubmit"){
				dispatchTopTextSubmittedEvent(unescape(response["text"]));
			} else if(e.code == "topTextInputClosed"){
                dispatchTopTextClosedEvent();
            } else if(e.code == "detailsViewDataRequest"){
                dispatchDetailsDataRequest(response["requestedData"]);
            } else if(e.code == "detailsViewClosed"){
                dispatchDetailsViewClosed();
            } else if(e.code == "detailsViewClosing"){
                dispatchDetailsViewClosing();
            } else if(e.code == "detailsItemSelected"){
                dispatchDetailsItemSelected(response["text"]);
            } else if(e.code == "userItemSelected"){
                dispatchUserItemSelected(response);
            } else if(e.code == "shareDidClose"){
                dispatchActionSheetClosedEvent(response["buttonIndex"]);
            } else if(e.code == "shareCurrentQuestionWithFriend"){
                dispatchShareCurrentQuestion(response["friendType"], response["friendId"])
            } else if(e.code == "sharedQuestionDeleted"){
                dispatchSharedQuestionDeleted(response["text"]);
            } else if (e.code == "viewDidChange"){
                dispatchViewDidChange(response["text"]);
            } else if(e.code == "detailsMultiItemSelected"){
                dispatchDetailsMultiItemSelected(response);
            }

			
		}

        private function dispatchDetailsMultiItemSelected(response:Object):void {
            var e:UserInputEvent = new UserInputEvent(UserInputEvent.DETAILS_MULTI_ITEM_SELECTED);
            e.data = response;
            dispatchEvent(e);
        }

        private function dispatchViewDidChange(view:String):void{
            var e:UserInputEvent = new UserInputEvent(UserInputEvent.VIEW_DID_CHANGE);
            e.text = view;
            dispatchEvent(e);
        }

        private function dispatchSharedQuestionDeleted(text:String):void {
            var e:UserInputEvent = new UserInputEvent(UserInputEvent.SHARED_QUESTION_DELETED);
            e.text = text;
            dispatchEvent(e);
        }

        private function dispatchShareCurrentQuestion(type:int, friendId:String):void{
            var e:UserInputEvent = null;
            if(type == 0){
                e = new UserInputEvent(UserInputEvent.SHARE_CURRENT_QUESTION_WITH_ICEBREAKER_FRIEND);
            } else if (type == 1){
                e = new UserInputEvent(UserInputEvent.SHARE_CURRENT_QUESTION_WITH_FACEBOOK_FRIEND);
            }

            e.text = friendId;
            dispatchEvent(e);
        }

        private function dispatchActionSheetClosedEvent(buttonIndex:Object):void{
            var e:UserInputEvent = new UserInputEvent(UserInputEvent.ACTION_SHEET_CLOSED);
            e.data = buttonIndex;
            dispatchEvent(e);
        }

        private function dispatchUserItemSelected(data:Object):void{
            var e:UserInputEvent = new UserInputEvent(UserInputEvent.USER_ITEM_SELECTED);
            e.data = data;
            dispatchEvent(e);
        }

        private function dispatchDetailsItemSelected(text:String):void{
            var e:UserInputEvent = new UserInputEvent(UserInputEvent.DETAILS_ITEM_SELECTED);
            e.text = text;
            dispatchEvent(e);

        }

        private function dispatchDetailsViewClosing():void {
            dispatchEvent(new UserInputEvent(UserInputEvent.DETAILS_VIEW_CLOSING));
        }

        private function dispatchDetailsViewClosed():void {
            dispatchEvent(new UserInputEvent(UserInputEvent.DETAILS_VIEW_CLOSED));
        }

        private function dispatchDetailsDataRequest(data:String):void {
           if(data == "user"){
               dispatchEvent(new UserInputEvent(UserInputEvent.USER_DATA_REQUESTED));
           } else if (data == "details"){
               dispatchEvent(new UserInputEvent(UserInputEvent.DETAILS_DATA_REQUESTED));
           } else if(data =="inbox"){
               dispatchEvent(new UserInputEvent(UserInputEvent.INBOX_DATA_REQUESTED));
           }else if(data =="share"){
               dispatchEvent(new UserInputEvent(UserInputEvent.SHARE_DATA_REQUESTED));
           } else if(data == "top"){
               dispatchEvent(new UserInputEvent(UserInputEvent.TOP_DATA_REQUESTED));
           }

        }
		
		private function dispatchTopTextSubmittedEvent(text:String):void{
			var e:UserInputEvent = new UserInputEvent(UserInputEvent.TOP_TEXT_INPUT_SUBMIT);
			e.text = text;
			dispatchEvent(e);
		}

        public function dispatchTopTextClosedEvent():void{
            var e:UserInputEvent = new UserInputEvent(UserInputEvent.TOP_TEXT_INPUT_CLOSED);
            dispatchEvent(e);
        }
		
		private function dispatchBottomTextSubmittedEvent(text:String):void{
			var e:UserInputEvent = new UserInputEvent(UserInputEvent.BOTTOM_TEXT_INPUT_SUBMIT);
			e.text = text;
			dispatchEvent(e);
		}
		
		private function dispatchDialogCloseEvent(didSubmit:Boolean, text:String):void{
			var e:UserInputEvent = new UserInputEvent(UserInputEvent.ON_DIALOG_CLOSE);
			e.didSubmit = didSubmit;
			e.text = text;
			dispatchEvent(e);
		}

		private function init():void{
			_extContext.call("init");
		}
		
		//top text input
		
		public function createTopTextInput():void{
			_extContext.call("createTopTextInput");
			_topTextInputCreated = true;
		}
		
		public function openTopTextInput():void{
			if(!_topTextInputCreated){
				trace("creating a new top text input with default settings");
				createTopTextInput();
			}
			_extContext.call("openTopTextInput");
		}
		
		public function closeTopTextInput():void{
			_extContext.call("closeTopTextInput");
		}
		
		public function removeTopTextInput():void{
			_topTextInputCreated = false; 
			_extContext.call("removeTopTextInput");
		}
		
		
		//bottom text input
		public function createBottomTextInput():void{
			_extContext.call("createBottomTextInput");
			_bottomTextInputCreated = true;
		}
		
		public function openBottomTextInput():void{
			if(!_bottomTextInputCreated){
				trace("creating a new bottom text input with default settings");
				createBottomTextInput();
			}
			_extContext.call("openBottomTextInput");
		}
		
		public function closeBottomTextInput():void{
			_extContext.call("closeBottomTextInput");
		}
		
		public function removeBottomTextInput():void{
			if(_bottomTextInputCreated){
                _bottomTextInputCreated = false;
			    _extContext.call("removeBottomTextInput");
            }
		}
		
		//share view controller
		public function openShareDialog():void{
			_extContext.call("openShareDialog");
		}

        //details view
        public function openDetailsView():void{
            _extContext.call("openDetailsView");
        }

        public function populateDetailsView(jsonData:String):void{
            _extContext.call("populateDetailsView", jsonData);
        }

        public function openUserDetailsView():void{
            _extContext.call("openUserDetailsView");
        }

        public function populateUserDetailsView(jsonData:String):void{
            _extContext.call("populateUserDetailsView", jsonData);
        }

        public function closeDetailsView():void{
            _extContext.call("closeDetailsView");
        }

        public function populateInboxView(jsonData:String):void{
            _extContext.call("populateInboxView", jsonData);
        }

        public function populateShareView(jsonData:String):void{
            _extContext.call("populateShareView", jsonData);
        }

        public function openFavoritesView():void{
            _extContext.call("openInboxView");
        }

        public function populateTopView(jsonData:String):void{
            _extContext.call("populateTopView", jsonData);
        }

        public function openTopView():void{
            _extContext.call("openTopView");
        }

        public function switchView(view:int):void{
            _extContext.call("switchView", view);
        }

        public function openActivityPopup():void{
            _extContext.call("openActivityPopup");
        }

        public function closeActivityPopup():void{
            _extContext.call("closeActivityPopup");
        }

        public function openActionSheet(title:String):void{
            _extContext.call("openActionSheet", title);
        }

        public function shareToUser(jsonData:String, fbUserId:String = null):void{
            if(fbUserId == null){
                _extContext.call("shareToCurrentUserWall", jsonData);
            } else {
                _extContext.call("shareToOtherUserWall", jsonData, fbUserId);
            }
        }

		
	}

}

class SingletonEnforcer{ }