DESTINATION=/Users/frank/Documents/projects/anes/UserInputANE/bin
XML=/Users/frank/Documents/projects/anes/UserInputANE/com/eyecu/ane/ios/userinput/extension.xml
SWC=/Users/frank/Documents/projects/anes/UserInputANE/com/eyecu/ane/ios/userinput/temp/UserInput.swc
ANE_NAME=UserInput.ane
IOS_LIB=/Users/frank/Documents/projects/anes/UserInputANE/com/eyecu/ane/ios/userinput/build/libUserInputANE.a
ADT=/Users/frank/Documents/as3_sdks/flex_4_11_4_0_beta_1/bin/adt
PLATFORM=/Users/frank/Documents/projects/anes/UserInputANE/com/eyecu/ane/ios/userinput/platform.xml

echo "removing $DESTINATION"
rm -r "$DESTINATION"
echo "remakeing dir"
mkdir -p "$DESTINATION"
mkdir -p "$DESTINATION"/build/ane/
mkdir -p "$DESTINATION"/build/ane/ios/

echo "Copy XML to new location"
cp "$XML" "$DESTINATION"/build/ane/	
	
cp "$SWC" "$DESTINATION"/build/ane/
unzip "$DESTINATION"/build/ane/*.swc -d "$DESTINATION"/build/ane
mv "$DESTINATION"/build/ane/library.swf "$DESTINATION"/build/ane/ios

cp "$IOS_LIB" "$DESTINATION"/build/ane/ios
"$ADT" -package -target ane "$DESTINATION/$ANE_NAME" "$DESTINATION"/build/ane/extension.xml -swc "$DESTINATION"/build/ane/*.swc -platform iPhone-ARM -platformoptions "$PLATFORM" -C "$DESTINATION"/build/ane/ios/ .

echo "complete"
