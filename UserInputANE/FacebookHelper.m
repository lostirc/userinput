//
// Created by frank on 12/29/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//

#import <FacebookSDK/FacebookSDK.h>
#import "FacebookHelper.h"


@implementation FacebookHelper {

}

+(void) shareToFacebookWall:(const uint8_t *) withData withDataLen:(uint32_t) dataLen{

    NSString * data = [FacebookHelper uintToNSString:withData withLen:dataLen];

    NSError * e;
    NSDictionary * shareResults = [NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding]
                                                      options:0
                                                        error:&e];

    [[FBSession activeSession] closeAndClearTokenInformation];
    NSArray* expectedPermissions = @[@"read_stream"];
    NSString * fbSessionId = [shareResults valueForKey:@"currentUserSessionId"];

    NSDictionary *dictionary = @{
            FBTokenInformationTokenKey:fbSessionId,
            FBTokenInformationExpirationDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
            FBTokenInformationPermissionsKey:expectedPermissions,
            FBTokenInformationLoginTypeLoginKey:[NSNumber numberWithInt:FBSessionLoginTypeFacebookViaSafari],
            FBTokenInformationRefreshDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
            FBTokenInformationPermissionsRefreshDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
    };

    FBAccessTokenData * tokenData = [FBAccessTokenData createTokenFromDictionary:dictionary];



    FBSession * session = [[[FBSession alloc] init] autorelease];

    [session openFromAccessTokenData:tokenData completionHandler:^(FBSession *completedSession, FBSessionState sessionState, NSError * sessionError){
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                [shareResults valueForKeyPath:@"name"], @"name",
                [shareResults valueForKey:@"caption"], @"caption",
                [shareResults valueForKey:@"content"], @"description",
                [shareResults valueForKey:@"facebookPostLink"], @"link",
                [shareResults valueForKey:@"imageUrl"], @"picture",
                nil];
        [FBWebDialogs presentFeedDialogModallyWithSession:completedSession parameters:params handler:nil];
    }];
}

+ (void)shareToFacebookWall:(const uint8_t *)withData withDataLen:(uint32_t)dataLen withOtherUser:(const uint8_t *)otherUser withOtherUserLen:(uint32_t)otherUserLen {
    NSString * data = [FacebookHelper uintToNSString:withData withLen:dataLen];

    NSError * e;
    NSDictionary * shareResults = [NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding]
                                                                  options:0
                                                                    error:&e];

    NSString * toUserId = [FacebookHelper uintToNSString:otherUser withLen:otherUserLen];

    [[FBSession activeSession] closeAndClearTokenInformation];
    NSArray* expectedPermissions = @[@"read_stream"];
    NSString * fbSessionId = [shareResults valueForKey:@"currentUserSessionId"];

    NSDictionary *dictionary = @{
            FBTokenInformationTokenKey:fbSessionId,
            FBTokenInformationExpirationDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
            FBTokenInformationPermissionsKey:expectedPermissions,
            FBTokenInformationLoginTypeLoginKey:[NSNumber numberWithInt:FBSessionLoginTypeFacebookViaSafari],
            FBTokenInformationRefreshDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
            FBTokenInformationPermissionsRefreshDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
    };

    FBAccessTokenData * tokenData = [FBAccessTokenData createTokenFromDictionary:dictionary];



    FBSession * session = [[[FBSession alloc] init] autorelease];

    [session openFromAccessTokenData:tokenData completionHandler:^(FBSession *completedSession, FBSessionState sessionState, NSError * sessionError){
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                [shareResults valueForKeyPath:@"name"], @"name",
                [shareResults valueForKey:@"caption"], @"caption",
                [shareResults valueForKey:@"content"], @"description",
                [shareResults valueForKey:@"facebookPostLink"], @"link",
                [shareResults valueForKey:@"imageUrl"], @"picture",
                toUserId, @"to",
                nil];
        [FBWebDialogs presentFeedDialogModallyWithSession:completedSession parameters:params handler:nil];
    }];


}


+(NSString *) uintToNSString:(const uint8_t *) str
                     withLen:(uint32_t) len{
    return [[[NSString alloc] initWithBytes:str length:len encoding:NSUTF8StringEncoding] autorelease];

}

@end