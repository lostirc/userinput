//
// Created by frank on 12/29/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface FacebookHelper : NSObject

+ (void)shareToFacebookWall:(const uint8_t *)withData withDataLen:(uint32_t)dataLen;

+ (void)shareToFacebookWall:(const uint8_t *)withData
                withDataLen:(uint32_t)dataLen
              withOtherUser:(const uint8_t *) otherUser
           withOtherUserLen:(uint32_t) otherUserLen;

@end