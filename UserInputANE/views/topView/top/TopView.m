//
// Created by frank on 12/8/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "TopView.h"



@implementation TopView {
    TopQuestionView * questionView;
    TopAnswersView * answerView;
    UsersView * usersView;

}

@synthesize delegate;

@synthesize lastSelected;

- (void)setTableData:(NSString *)tableJson {
    NSError * e;
    NSDictionary * data = [NSJSONSerialization JSONObjectWithData:[tableJson dataUsingEncoding:NSUTF8StringEncoding]
                                                      options:0
                                                        error:&e];


    [answerView setTableData:[data valueForKeyPath:@"top.topAnswers"]];
    [questionView setTableData:[data valueForKeyPath:@"top.topQuestions"]];
    [usersView setTableData:[data valueForKeyPath:@"top.topUsers"]];

}


- (void)setupInterface {
    questionView = [[[TopQuestionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [questionView setupInterface];
    [questionView setHidden:NO];
    questionView.delegate = self;
    [self addSubview:questionView];

    answerView = [[[TopAnswersView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [answerView setupInterface];
    [answerView setHidden:YES];
    answerView.delegate = self;
    [self addSubview:answerView];

    usersView = [[[UsersView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [usersView setupInterface];
    [usersView setHidden:YES];
    usersView.delegate = self;
    [self addSubview:usersView];



}

-(void) handleSwitchData:(id) sender{
    UISegmentedControl * control = sender;
    [questionView setHidden: [control selectedSegmentIndex] != 0 ? YES : NO];
    [answerView setHidden: [control selectedSegmentIndex] != 1 ? YES : NO];
    [usersView setHidden: [control selectedSegmentIndex] != 2 ? YES : NO];
    lastSelected = [control selectedSegmentIndex];

}

- (void)detailsListItemSelected:(NSString *)item {
    if([delegate respondsToSelector:@selector(detailsItemSelected:)]){
        [delegate detailsItemSelected:item];
    }
}

- (void)detailsUserItemSelected:(NSString *)item {
    if([delegate respondsToSelector:@selector(userItemSelected:)]){
        [delegate userItemSelected:item];
    }
}

- (void)detailsMultiValueItemSelected:(NSDictionary *)item {
    if([delegate respondsToSelector:@selector(detailsMultiItemSelected:)]){
        [delegate detailsMultiItemSelected:item];
    }
}


@end