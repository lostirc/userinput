//
// Created by frank on 12/8/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "QuestionAnswerDetailsView.h"
#import "DetailsItemSelectedDelegate.h"
#import "UserDetailsView.h"
#import "UsersView.h"
#import "TopQuestionView.h"
#import "TopAnswersView.h"

@interface TopView : UIView<DetailsItemSelectedDelegate>{
    id<QuestionAnswerDetailsViewDelegate, UserDetailsViewDelegate> delegate;
}

- (void)setTableData: (NSString *) tableJson;
- (void)setupInterface;

- (void)handleSwitchData:(id)sender;

@property (nonatomic, assign)id delegate;

@property int lastSelected;

@end