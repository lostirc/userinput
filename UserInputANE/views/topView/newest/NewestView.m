//
// Created by frank on 3/12/14.
// Copyright (c) 2014 Eyecu. All rights reserved.
//


#import "NewestView.h"



@implementation NewestView {
    TopQuestionView * questionView;
    TopAnswersView * answerView;
}

@synthesize delegate;

@synthesize lastSelected;

- (void)setTableData:(NSString *)tableJson {
    NSError * e;
    NSDictionary * data = [NSJSONSerialization JSONObjectWithData:[tableJson dataUsingEncoding:NSUTF8StringEncoding]
                                                          options:0
                                                            error:&e];


    [answerView setTableData:[data valueForKeyPath:@"newest.topAnswers"]];
    [questionView setTableData:[data valueForKeyPath:@"newest.topQuestions"]];

}

- (void)setupInterface {
    questionView = [[[TopQuestionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [questionView setupInterface];
    [questionView setHidden:NO];
    questionView.delegate = self;
    [self addSubview:questionView];

    answerView = [[[TopAnswersView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [answerView setupInterface];
    [answerView setHidden:YES];
    answerView.delegate = self;
    [self addSubview:answerView];
}

-(void) handleSwitchData:(id) sender{
    UISegmentedControl * control = sender;
    [questionView setHidden: [control selectedSegmentIndex] != 0 ? YES : NO];
    [answerView setHidden: [control selectedSegmentIndex] != 1 ? YES : NO];
    lastSelected = [control selectedSegmentIndex];
}

- (void)detailsListItemSelected:(NSString *)item {
    if([delegate respondsToSelector:@selector(detailsItemSelected:)]){
        [delegate detailsItemSelected:item];
    }
}

- (void)detailsMultiValueItemSelected:(NSDictionary *)item {
    if([delegate respondsToSelector:@selector(detailsMultiItemSelected:)]){
        [delegate detailsMultiItemSelected:item];
    }
}


@end