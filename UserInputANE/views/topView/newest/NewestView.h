//
// Created by frank on 3/12/14.
// Copyright (c) 2014 Eyecu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DetailsItemSelectedDelegate.h"
#import "TopQuestionView.h"
#import "TopAnswersView.h"
#import "QuestionAnswerDetailsView.h"

@interface NewestView : UIView<DetailsItemSelectedDelegate>{
    id<QuestionAnswerDetailsViewDelegate> delegate;
}

- (void)setTableData: (NSString *) tableJson;
- (void)setupInterface;

- (void)handleSwitchData:(id)sender;

@property (nonatomic, assign)id delegate;

@property int lastSelected;

@end