//
// Created by frank on 3/12/14.
// Copyright (c) 2014 Eyecu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ViewDidChangeDelegate.h"
#import "TopView.h"
#import "NewestView.h"

@protocol ViewDidChangeDelegate;


@interface TopViewContainer : UIView<UIScrollViewDelegate>{
    id<ViewDidChangeDelegate> delegate;
}

- (BOOL)checkForDataLoadNeeded;
- (void)setTableData: (NSString *) tableJson withResponder: (id) responder;
- (void)setupInterface;

@property (nonatomic, assign) id delegate;

@end