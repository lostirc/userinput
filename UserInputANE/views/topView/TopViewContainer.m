//
// Created by frank on 3/12/14.
// Copyright (c) 2014 Eyecu. All rights reserved.
//

#import "TopViewContainer.h"




@implementation TopViewContainer {
    UIPageControl * _pageControl;
    TopView * _topView;
    NewestView * _newestView;
    UIScrollView * _scrollView;
    UISegmentedControl * _gridControl;

}

@synthesize delegate;

- (BOOL)checkForDataLoadNeeded {
    return YES;
}

- (void)setTableData:(NSString *)tableJson withResponder: (id) responder  {
    [_topView setTableData:tableJson];
    [_topView setDelegate:responder];
    [_newestView setTableData:tableJson];
    [_newestView setDelegate:responder];
}

- (void)setupInterface {

    _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 8, self.frame.size.width, self.frame.size.height - 8)];
    [self addSubview:_scrollView];
    [_scrollView setCanCancelContentTouches:NO];
    [_scrollView setIndicatorStyle:UIScrollViewIndicatorStyleWhite];
    [_scrollView setClipsToBounds:YES];
    [_scrollView setPagingEnabled:YES];

    [_scrollView setContentSize:CGSizeMake(self.frame.size.width * 2, self.frame.size.height - 8)];
    _scrollView.delegate = self;


    _topView = [[TopView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 8)];
    [_topView setupInterface];
    [_scrollView addSubview:_topView];

    _newestView = [[NewestView alloc] initWithFrame:CGRectMake(self.frame.size.width, 0, self.frame.size.width, self.frame.size.height - 8)];
    [_newestView setupInterface];
    [_scrollView addSubview:_newestView];



    UIView * headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 43)] autorelease];
    [headerView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [headerView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [headerView.layer setShadowOffset:CGSizeMake(0, 5)];
    [headerView.layer setShadowOpacity:0.5];

    [self addSubview:headerView];


    _gridControl = [[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Questions", @"Answers", @"Users", nil]];

    [_gridControl addTarget:_topView action:@selector(handleSwitchData:) forControlEvents:UIControlEventValueChanged];
    [_gridControl setFrame:CGRectMake(5, 2, self.frame.size.width-10, 30)];
    [_gridControl setTintColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];


    [_gridControl setSelectedSegmentIndex:0];

    [self addSubview:_gridControl];

    _pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake( (self.frame.size.width/2) - 50 , 35, 100, 5)];
    [_pageControl setNumberOfPages:2];
    [headerView addSubview:_pageControl];


}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = _scrollView.frame.size.width;
    int page = (int) (floor((_scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1);

    if(page != [_pageControl currentPage]){
        [self viewChange:page];
    }
    [_pageControl setCurrentPage:page];
}

-(void) viewChange:(int) view{
    NSLog(@"changing view %i", view);
    if(view == 1){
        [_gridControl removeTarget:_topView action:@selector(handleSwitchData:) forControlEvents:UIControlEventValueChanged];
        [_gridControl addTarget:_newestView action:@selector(handleSwitchData:) forControlEvents:UIControlEventValueChanged];
        [_gridControl setEnabled:NO forSegmentAtIndex:2];
        [_gridControl setSelectedSegmentIndex:[_newestView lastSelected]];
        [self dispatchViewChange:@"newest"];
        [_newestView handleSwitchData:_gridControl];
    } else if (view == 0) {
        [_gridControl removeTarget:_newestView action:@selector(handleSwitchData:) forControlEvents:UIControlEventValueChanged];
        [_gridControl addTarget:_topView action:@selector(handleSwitchData:) forControlEvents:UIControlEventValueChanged];
        [_gridControl setEnabled:YES forSegmentAtIndex:2];
        [_gridControl setSelectedSegmentIndex:[_topView lastSelected]];
        [self dispatchViewChange:@"top"];
        [_topView handleSwitchData:_gridControl];
    }
}

-(void) dispatchViewChange:(NSString *) view{
    if([delegate respondsToSelector:@selector(viewDidChange:)]){
        [delegate viewDidChange:view];
    }
}

- (void)dealloc {
    [_pageControl release];
    [_topView release];
    [_newestView release];
    [_scrollView release];
    [_gridControl release];
    [super dealloc];
}


@end