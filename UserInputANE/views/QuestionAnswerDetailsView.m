//
// Created by frank on 11/8/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "QuestionAnswerDetailsView.h"



@implementation QuestionAnswerDetailsView {
    AnswerView * answerView;
    QuestionView * questionView;
    FavoritesView * favoritesView;
}
@synthesize delegate;


- (void)setupInterface{

    questionView = [[[QuestionView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [questionView setupInterface];
    questionView.delegate = self;
    [questionView setHidden:NO];
    [self addSubview:questionView];

    answerView = [[[AnswerView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [answerView setupInterface];
    [answerView setHidden:YES];
    answerView.delegate = self;
    [self addSubview:answerView];

    favoritesView = [[[FavoritesView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height - 30)] autorelease];
    [favoritesView setupInterface];
    [favoritesView setHidden:YES];
    favoritesView.delegate = self;
    [self addSubview:favoritesView];

    UIView * headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 34)] autorelease];
    [headerView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [headerView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [headerView.layer setShadowOffset:CGSizeMake(0, 5)];
    [headerView.layer setShadowOpacity:0.5];

    [self addSubview:headerView];


    UISegmentedControl * gridControl = [[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Questions", @"Answers", @"Favorites", nil]] autorelease];
    NSString * heartImageLoc = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"heart_small_off"]
                                                               ofType:@".png"
                                                          inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

    UIImage * heartImage = [[UIImage imageWithContentsOfFile:heartImageLoc] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                                resizingMode:UIImageResizingModeTile];
    [gridControl setImage:heartImage forSegmentAtIndex:2];
    [gridControl addTarget:self action:@selector(handleSwitchData:) forControlEvents:UIControlEventValueChanged];
    [gridControl setFrame:CGRectMake(5, 2, self.frame.size.width-10, 30)];
    [gridControl setTintColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];


    [gridControl setSelectedSegmentIndex:0];

    [self addSubview:gridControl];



}

-(BOOL) checkForDataLoadNeeded{
    return [answerView.loadedData count] == 0;
}

-(void) handleSwitchData:(id) sender{
    UISegmentedControl * control = sender;
    [questionView setHidden: [control selectedSegmentIndex] != 0 ? YES : NO];
    [answerView setHidden: [control selectedSegmentIndex] != 1 ? YES : NO];
    [favoritesView setHidden: [control selectedSegmentIndex] != 2 ? YES : NO];

}

- (void) setTableData:(NSString *)tableJson {

    NSError * e;

    NSDictionary * data =  [NSJSONSerialization JSONObjectWithData:[tableJson dataUsingEncoding:NSUTF8StringEncoding]
                                                       options:0
                                                         error:&e];

    NSLog(@"details view data: %@", data);

    [answerView setTableData:[data valueForKey:@"answers"]];
    [questionView setTableData:[data valueForKey:@"questions"]];
    [favoritesView setTableData:[data valueForKey:@"favorites"]];

}

- (void)detailsListItemSelected:(NSString *)item {
    if([delegate respondsToSelector:@selector(detailsItemSelected:)]){
        [delegate detailsItemSelected:item];
    }
}

- (void)detailsMultiValueItemSelected:(NSDictionary *)item {
    if([delegate respondsToSelector:@selector(detailsMultiItemSelected:)]){
        [delegate detailsMultiItemSelected:item];
    }
}


@end