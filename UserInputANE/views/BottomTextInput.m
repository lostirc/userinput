//
// Created by frank on 10/7/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "BottomTextInput.h"


@implementation BottomTextInput {

    UITextViewWithPlaceholder * input;
    UIView * inputView;
    UIView * parentView;
    UITapGestureRecognizer * closeTextInputRecognizer;
}

@synthesize delegate;



-(void) openBottomTextInput {


    [UIView beginAnimations:nil context:nil];

    [UIView setAnimationDuration:0.3];

    [UIView setAnimationBeginsFromCurrentState:YES];

    CGRect startFrame = CGRectMake(0, parentView.bounds.size.height, parentView.bounds.size.width, 39);

    startFrame.origin.y = parentView.frame.size.height - 39;
    inputView.frame = startFrame;


    [UIView commitAnimations];


}

-(void) closeBottomTextInput {

    [input resignFirstResponder];
    CGRect startFrame = CGRectMake(inputView.frame.origin.x, inputView.frame.origin.y, inputView.frame.size.width, inputView.frame.size.height);
    startFrame.origin.y += 39;

    [UIView animateWithDuration:0.3 animations:^{
        inputView.frame = startFrame;
    }];

}

-(void)dealloc {
    NSLog(@"dealloc bottomTextInput");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [inputView removeFromSuperview];
    [super dealloc];
}

-(void) allocBottomTextInput:(UIView *) view{

    parentView = view;
    //text input
    input = [[[UITextViewWithPlaceholder alloc] initWithFrame:CGRectMake(2, 2, view.bounds.size.width - 4, 35)] autorelease];

    [input.layer setCornerRadius:2];
    [input setKeyboardType:UIKeyboardTypeAlphabet];
    [input setReturnKeyType:UIReturnKeySend];
    [input setPlaceholder:@"Answer?"];
    [input setFont:[UIFont systemFontOfSize:15]];
    [input setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];


    input.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];


    CGRect startFrame = CGRectMake(0, view.bounds.size.height, view.bounds.size.width, 39);

    inputView = [[[UIView alloc] initWithFrame:startFrame] autorelease];

    [inputView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];


    [inputView addSubview:input];


    [view addSubview:inputView];

}


- (void)doSubmit {

    if([delegate respondsToSelector:@selector(bottomTextSubmitted:)]){
        [delegate bottomTextSubmitted:input.text];
    }

    [self dismissKeyboard];
}



//UITextView delegate

- (void)textViewDidChange:(UITextView *)textView {
    CGRect inputFrame = input.frame;
    CGFloat newHeight = [self textViewHeightForAttributedText:[input attributedText] andWidth:inputFrame.size.width];
    inputFrame.size.height = newHeight;

    CGRect inputViewFrame = inputView.frame;
    inputViewFrame.size.height = newHeight + 4;
    inputViewFrame.origin.y = [self inputViewYForHeight:newHeight];



    [UIView animateWithDuration:.2 animations:^{
        input.frame = inputFrame;
        inputView.frame = inputViewFrame;

    }];
}

- (CGFloat)inputViewYForHeight: (CGFloat) newHeight{

    return (self.inputLocation.origin.y + 39) - (newHeight + 4);
}

- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width {
    UITextView *calculationView = [[[UITextView alloc] init] autorelease];
    [calculationView setAttributedText:text];
    CGSize size = [calculationView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height > 35 ? size.height : 35;
}


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    if([text isEqualToString:@"\n"]){
        [self doSubmit];

        if([[input text] length] != 0){
            [self doSubmit];
        } else {
            [self dismissKeyboard];
        }

        return NO;
    }


    NSInteger newLength = [textView.text length] + [text length] - range.length;
    return newLength <= 200;
}


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {

    closeTextInputRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)] autorelease];
    [parentView addGestureRecognizer:closeTextInputRecognizer];

    [UIView animateWithDuration:0.3 animations:^{
        input.frame = CGRectMake(input.frame.origin.x, input.frame.origin.y, input.frame.size.width, input.frame.size.height);
    }
    completion:^(BOOL finished){
        self.inputLocation = inputView.frame;
    }];

    return YES;
}
- (BOOL)textViewShouldEndEditing:(UITextView *)textView {

    [parentView removeGestureRecognizer:closeTextInputRecognizer];

    [UIView animateWithDuration:0.3 animations:^{
        input.frame = CGRectMake(input.frame.origin.x, input.frame.origin.y, input.frame.size.width, 35);
        inputView.frame = self.inputLocation;

    }];

    return YES;
}

//keyboard and input view animations



-(void)keyboardWillShow:(NSNotification *) notification{

    [self doAnimation:YES withNotifcation:notification];
}

-(void)keyboardWillHide:(NSNotification *)notification {

    [self doAnimation:NO withNotifcation:notification];
}

-(void) dismissKeyboard{
    NSLog(@"dismissKeyboard bottomTextInput");
    [input setText:@""];
    [input resignFirstResponder];
}


-(void)doAnimation:(BOOL) open withNotifcation:(NSNotification *) notification{

    NSValue * keyboardFrame = [[notification userInfo] valueForKey:UIKeyboardFrameBeginUserInfoKey];
    CGRect keyFrame = [keyboardFrame CGRectValue];

    NSNumber * duration = [[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey];


    [UIView beginAnimations:nil context:nil];

    [UIView setAnimationDuration:[duration doubleValue]];

    [UIView setAnimationCurve:(UIViewAnimationCurve) [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue]];

    [UIView setAnimationBeginsFromCurrentState:YES];

    CGRect offsetFrame = inputView.frame;

    if(open){
        offsetFrame.origin.y -= keyFrame.size.height;
    } else {
        offsetFrame.origin.y += keyFrame.size.height;
    }


    inputView.frame = offsetFrame;

    [UIView commitAnimations];


}

@end