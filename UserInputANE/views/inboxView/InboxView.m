//
// Created by frank on 11/23/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "InboxView.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"


@implementation InboxView {
    BOOL inAllView;
    UISegmentedControl * gridControl;
    UITableView * sharedTable;
}

@synthesize loadedData = _loadedData;
@synthesize delegate;
@synthesize unseenData = _unseenData;

- (void)setupInterface {
    sharedTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, self.frame.size.height - 60) style:UITableViewStyleGrouped];

    [sharedTable setBackgroundColor:[[[UIColor alloc] initWithWhite:1 alpha:0.0] autorelease]];
    [sharedTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [sharedTable setTableHeaderView:[[[UIView alloc] initWithFrame:CGRectMake(0, 0, sharedTable.frame.size.width, 10)] autorelease]];
    [sharedTable registerClass:[SharedQuestionCell class] forCellReuseIdentifier:@"sharedCell"];
    sharedTable.delegate = self;
    [sharedTable setDataSource:self];
    [self addSubview:sharedTable];

    UIView * headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 34)] autorelease];
    [headerView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [headerView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [headerView.layer setShadowOffset:CGSizeMake(0, 5)];
    [headerView.layer setShadowOpacity:0.5];

    [self addSubview:headerView];

    gridControl = [[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"All", @"New", nil]] autorelease];
    [gridControl addTarget:self action:@selector(handleSwitchData) forControlEvents:UIControlEventValueChanged];
    [gridControl setFrame:CGRectMake(2, 2, self.frame.size.width - 4, 30)];
    [gridControl setTintColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];


    [gridControl setSelectedSegmentIndex:0];
    [headerView addSubview:gridControl];

}




- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 2;
}


-(void) handleSwitchData{
    inAllView = [gridControl selectedSegmentIndex] == 0;
    [sharedTable reloadData];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSLog(@"delete");
        if(inAllView){
            [self dispatchSharedQuestionRemoved:[[_loadedData objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"pk"]];
            [_loadedData removeObjectAtIndex:(NSUInteger) indexPath.section];
            //remove from unseen data
            //check and set index
            NSIndexSet * indexSet = [_loadedData indexesOfObjectsPassingTest:^BOOL(id element, NSUInteger i, BOOL *stop) {
                NSNumber * seen = [element valueForKey:@"seen"];
                return [seen isEqualToNumber:[NSNumber numberWithChar:0]];

            }];

            if([indexSet count] != 0){
                self.unseenData = [NSMutableArray arrayWithArray:[_loadedData objectsAtIndexes:indexSet]];
            }

            if([self.unseenData count] == 0){
                [gridControl setEnabled:NO forSegmentAtIndex:1];
            }

            [sharedTable deleteSections:[NSIndexSet indexSetWithIndex:(NSUInteger) indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];

        } else {
            id toRemove = [_unseenData objectAtIndex:(NSUInteger) indexPath.section];
            [self dispatchSharedQuestionRemoved:[toRemove valueForKeyPath:@"pk"]];
            [_unseenData removeObjectAtIndex:(NSUInteger) indexPath.section];
            [_loadedData removeObject:toRemove];
            [sharedTable deleteSections:[NSIndexSet indexSetWithIndex:(NSUInteger) indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            if([_unseenData count] == 0){
                [gridControl setSelectedSegmentIndex:0];
                [gridControl setEnabled:NO forSegmentAtIndex:1];
                [self handleSwitchData];
            }
        }

    }
}


-(void) dispatchSharedQuestionRemoved: (NSString *) questionId {
    if([delegate respondsToSelector:@selector(sharedQuestionDeleted:)]){
        [delegate sharedQuestionDeleted:questionId];
    }
}
- (void)setTableData:(NSString *)jsonData {
    NSError * e;
    self.loadedData = [NSMutableArray arrayWithArray:[NSJSONSerialization JSONObjectWithData:[jsonData dataUsingEncoding:NSUTF8StringEncoding]
                                                  options:0
                                                    error:&e]];


    NSIndexSet * indexSet = [_loadedData indexesOfObjectsPassingTest:^BOOL(id element, NSUInteger i, BOOL *stop) {
        NSNumber * seen = [element valueForKey:@"seen"];
        return [seen isEqualToNumber:[NSNumber numberWithChar:0]];

    }];

    if([indexSet count] != 0){
        self.unseenData = [NSMutableArray arrayWithArray:[_loadedData objectsAtIndexes:indexSet]];
    }

    if([self.unseenData count] != 0){
        [gridControl setSelectedSegmentIndex:1];
    } else {
        [gridControl setSelectedSegmentIndex:0];
        [gridControl setEnabled:NO forSegmentAtIndex:1];
    }
    [self handleSwitchData];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
   return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if(inAllView){
        return [_loadedData count];
    } else {
        return [_unseenData count];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellId = @"sharedCell";
    SharedQuestionCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];

    NSArray * dataToCheck = (inAllView) ?  _loadedData : _unseenData;
    /**
    * Shared by details
    */
    NSString *imgUrl = [NSString stringWithFormat:@"%@?width=100&height=100",
    [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"sharedBy.photo"]];

    [cell.imageView setImageWithURL:[NSURL URLWithString:imgUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    NSString * facebookName = [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"sharedBy.name"];
    NSString * score = [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"sharedBy.score"];

    [cell.textLabel setText:[NSString stringWithFormat:@"%@ (%@)", facebookName, score]];


    [cell.detailTextLabel setText:@"Shared by"];

    /**
    * Question text
    */

    imgUrl = [NSString stringWithFormat:@"%@?width=100&height=100",
    [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.user.photo"]];

    [cell.questionImageView setImageWithURL:[NSURL URLWithString:imgUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    [cell.questionDetailsLabel setText:[NSString stringWithFormat:@"%@ (%@)\nQuestion Score (%@)",
                                       [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.user.name"],
                                       [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.user.score"],
                                       [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.score"]]];

    [cell setQuestionText:[[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.question"]];
    NSNumber * seen = [[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKey:@"seen"];
    [cell setNewQuestionBorder:[seen isEqualToNumber:[NSNumber numberWithChar:0]]];

    BOOL lock = [[[dataToCheck objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.closed"] boolValue];
    [cell lockQuestion:lock];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGSize maximumQuestionLabelSize = CGSizeMake(self.frame.size.width - 20, FLT_MAX);
    NSString * question = inAllView ?
            [[_loadedData objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.question"] :
            [[_unseenData objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.question"];

    return [SizeUtil text:question heightWithFont:[UIFont systemFontOfSize:14.0] constrainedToSize:maximumQuestionLabelSize] + 113;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * id;

    if(inAllView){
        id = [[_loadedData objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.id"];
    } else {
        id = [[_unseenData objectAtIndex:(NSUInteger) indexPath.section] valueForKeyPath:@"question.id"];
    }

    if([delegate respondsToSelector:@selector(detailsItemSelected:)]){
        [delegate detailsItemSelected:id];
    }
}


- (void)dealloc {
    [_unseenData release];
    [sharedTable release];
    [super dealloc];
}


@end