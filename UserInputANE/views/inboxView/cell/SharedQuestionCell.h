//
// Created by frank on 11/23/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SharedQuestionCell : UITableViewCell

@property (nonatomic, strong) UIView * questionView;
@property (nonatomic, strong) UIImageView * questionImageView;
@property (nonatomic, strong) UILabel * questionDetailsLabel;
@property (nonatomic, strong) UILabel * questionLabel;
@property (nonatomic, strong) UIImageView * imageView;


- (void)setNewQuestionBorder:(BOOL)on;

- (void)setQuestionText:(NSString *)question;

- (void)lockQuestion:(BOOL)lock;
@end