//
// Created by frank on 11/23/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "SharedQuestionCell.h"
#import "SizeUtil.h"

@interface SharedQuestionCell()
    @property(nonatomic, retain) UIImageView * lockIconView;
@end

@implementation SharedQuestionCell {

}

@synthesize questionImageView = _questionImageView;

@synthesize questionView = _questionView;

@synthesize questionDetailsLabel = _questionDetailsLabel;

@synthesize questionLabel = _questionLabel;

@synthesize imageView = _imageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {


        [self setBackgroundColor:[UIColor clearColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setIndentationWidth:0.0];
        /**
       * User details view
       */
        NSString * backgroundLoc = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"viewBackground"]
                                                                   ofType:@".png"
                                                              inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

        UIImage * backgroundImage = [[UIImage imageWithContentsOfFile:backgroundLoc] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                                    resizingMode:UIImageResizingModeTile];



        UIView * backgroundImageFill = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 54)] autorelease];
        [backgroundImageFill setTag:1];
        [backgroundImageFill.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [backgroundImageFill.layer setBorderWidth:1];
        [backgroundImageFill.layer setCornerRadius:2];
        [backgroundImageFill setClipsToBounds:YES];
        [backgroundImageFill setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];

        [self.contentView addSubview:backgroundImageFill];

        /**
        * Question View
        */

        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 50, 50)];
        [self.contentView addSubview:_imageView];

        _questionView = [[UIView alloc] initWithFrame:CGRectMake(0, 53, self.frame.size.width, 54)];
        [_questionView.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [_questionView.layer setBorderWidth:1];
        [_questionView.layer setCornerRadius:2];
        [_questionView setClipsToBounds:YES];
        [_questionView setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];

        _questionImageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 50, 50)];
        [_questionImageView.layer setCornerRadius:2];
        [_questionImageView setClipsToBounds:YES];

        [_questionView addSubview:_questionImageView];

        _questionDetailsLabel = [[UILabel alloc] initWithFrame:CGRectMake(54, 0, self.frame.size.width - 56, 50)];
        [_questionDetailsLabel setTextAlignment:NSTextAlignmentCenter];
        [_questionDetailsLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_questionDetailsLabel setFont:[UIFont boldSystemFontOfSize:17.0]];
        [_questionDetailsLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
        [_questionDetailsLabel setNumberOfLines:0];

        [_questionView addSubview:_questionDetailsLabel];

        _questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 54, self.frame.size.width - 4, 0)];
        [_questionLabel setTextAlignment:NSTextAlignmentCenter];
        [_questionLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_questionLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_questionLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
        [_questionLabel setNumberOfLines:0];

        [_questionView addSubview:_questionLabel];

        [self.contentView addSubview:_questionView];

        NSString * lockIcon = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"lock"]
                                                              ofType:@".png"
                                                         inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

        UIImage * lockImage = [[UIImage imageWithContentsOfFile:lockIcon] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                         resizingMode:UIImageResizingModeStretch];

        _lockIconView = [[[UIImageView alloc] initWithImage:lockImage] autorelease];
        [_lockIconView setFrame:CGRectMake(52, 37, 12, 15)];
        [_lockIconView setHidden:YES];
        [self.contentView addSubview:_lockIconView];
    }

    return self;
}




-(void) setNewQuestionBorder:(BOOL) on{
    UIView * backgroundImageFill = [self.contentView viewWithTag:1];
    if(on){
        [backgroundImageFill.layer setBorderColor:[UIColor colorWithRed:255/255.0f green:204/255.0f blue:0/255.0f alpha:1.0f].CGColor];
        [_questionView.layer setBorderColor:[UIColor colorWithRed:255/255.0f green:204/255.0f blue:0/255.0f alpha:1.0f].CGColor];

    }
    else {
        [backgroundImageFill.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [backgroundImageFill.layer setBorderWidth:1];
        [_questionView.layer  setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [_questionView.layer  setBorderWidth:1];

    }
}

-(void) setQuestionText:(NSString *) question{
    [_questionLabel setText:question];

    CGSize maximumQuestionLabelSize = CGSizeMake(self.frame.size.width - 10, FLT_MAX);
    CGRect newViewFrame = _questionLabel.frame;
    newViewFrame.size.height = [SizeUtil text:question heightWithFont:_questionLabel.font constrainedToSize:maximumQuestionLabelSize];
    [_questionLabel setFrame:newViewFrame];

    newViewFrame = _questionView.frame;
    newViewFrame.size.height = _questionLabel.frame.origin.y + _questionLabel.frame.size.height + 6;
    [_questionView setFrame:newViewFrame];

}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 5;
    frame.size.width -= 10;

    [super setFrame:frame];
}

- (void)layoutSubviews {
    [super layoutSubviews];


    _imageView.frame = CGRectMake(2, 2, 50, 50);
    [_imageView.layer setCornerRadius:2];
    [_imageView setClipsToBounds:YES];

    [self.textLabel setFrame:CGRectMake(55, 20, self.frame.size.width - 55, 34)];
    [self.textLabel setBackgroundColor:[UIColor clearColor]];
    [self.textLabel setFont:[UIFont boldSystemFontOfSize:17.0]];
    [self.textLabel setTextAlignment: NSTextAlignmentCenter];
    [self.textLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];

    [self.detailTextLabel setFrame:CGRectMake(55, 5, self.frame.size.width - 55, 15)];
    [self.detailTextLabel setBackgroundColor:[UIColor clearColor]];
    [self.detailTextLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
    [self.detailTextLabel setTextAlignment: NSTextAlignmentCenter];
    [self.detailTextLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
}

- (void)dealloc {
    [_questionView release];
    [_questionImageView release];
    [_questionDetailsLabel release];
    [_questionLabel release];
    [_imageView release];
    [super dealloc];
}

- (void)lockQuestion:(BOOL) lock{
    if(lock){
        [_lockIconView setHidden:NO];
    } else {
        [_lockIconView setHidden:YES];
    }
}

@end