//
// Created by frank on 11/23/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DetailsItemSelectedDelegate.h"
#import "SharedQuestionCell.h"
#import "QuestionAnswerDetailsView.h"
@protocol SharedQuestionDeletedDelegate<NSObject>
    @optional
    -(void) sharedQuestionDeleted:(NSString *) item;
@end

@interface InboxView : UIView <UITableViewDelegate, UITableViewDataSource>{
    id<QuestionAnswerDetailsViewDelegate, SharedQuestionDeletedDelegate> delegate;
}
@property (nonatomic, assign)id delegate;
@property (nonatomic, strong) NSMutableArray * loadedData;
@property (nonatomic, strong) NSMutableArray * unseenData;

- (void)setupInterface;
- (void)setTableData: (NSString *) jsonData;

@end