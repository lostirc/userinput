//
// Created by frank on 12/11/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UserDetailsTableViewCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "DetailsItemSelectedDelegate.h"


@interface UsersView : UIView <UITableViewDelegate, UITableViewDataSource>{
    id<DetailsItemSelectedDelegate> delegate;
}

@property (nonatomic, assign)id delegate;
@property (nonatomic, strong) NSArray * loadedData;

- (void)setupInterface;
- (void)setTableData: (NSArray *) data;

@end