//
// Created by frank on 12/11/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//

#import "UsersView.h"



@implementation UsersView {
    UITableView * usersTable;

}
@synthesize delegate;

- (void)setupInterface {
    usersTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)] autorelease];
    [usersTable setBackgroundColor:[[[UIColor alloc] initWithWhite:1 alpha:0.0] autorelease]];
    [usersTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [usersTable registerClass:[UserDetailsTableViewCell class] forCellReuseIdentifier:@"topUsersDetailCell"];
    usersTable.delegate = self;
    [usersTable setDataSource:self];
    [usersTable setTableHeaderView:[[[UIView alloc] initWithFrame:CGRectMake(0, 0, usersTable.frame.size.width, 40)] autorelease]];
    [self addSubview:usersTable];
}

- (void)setTableData:(NSArray *)data {
    self.loadedData = data;
    [usersTable reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.loadedData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellId = @"topUsersDetailCell";
    UserDetailsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];

    NSString * imageUrl =[NSString stringWithFormat:@"%@?width=100&height=100", [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"photo"]];
    [cell.imageView setImageWithURL:[NSURL URLWithString:imageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];



    NSString * facebookName = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"name"];
    NSString * score = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"score"];

    [cell.textLabel setText:[NSString stringWithFormat:@"%@ (%@)", facebookName, score]];

    //todo: add something under the username, not sure what yet


//    NSString * totals = [NSString stringWithFormat:@"questions: %@ answers: %@",
//                                                   [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"questionsTotal"],
//                                                   [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"answersTotal"]];


//    [cell.detailTextLabel setText:totals];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 57;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSError * e;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] options:NSJSONWritingPrettyPrinted error:&e];
    NSString * jsonString = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];

    if([delegate respondsToSelector:@selector(detailsUserItemSelected:)]){
        [delegate detailsUserItemSelected:jsonString];
    }
}


@end