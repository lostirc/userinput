//
// Created by frank on 11/26/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UserDetailsTableViewCell : UITableViewCell

@property (nonatomic, strong) UIImageView * imageView;

@end