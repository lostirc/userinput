//
// Created by frank on 11/26/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "UserDetailsTableViewCell.h"


@implementation UserDetailsTableViewCell {

}
@synthesize imageView = _imageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {

        [self setBackgroundColor:[UIColor clearColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setIndentationWidth:0.0];

        NSString * backgroundLoc = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"viewBackground"]
                                                                   ofType:@".png"
                                                              inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

        UIImage * backgroundImage = [[UIImage imageWithContentsOfFile:backgroundLoc] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                                    resizingMode:UIImageResizingModeTile];

        UIView * backgroundImageFill = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 54)] autorelease];
        [backgroundImageFill setTag:1];
        [backgroundImageFill.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [backgroundImageFill.layer setBorderWidth:1];
        [backgroundImageFill.layer setCornerRadius:2];
        [backgroundImageFill setClipsToBounds:YES];
        [backgroundImageFill setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];

        [self.contentView addSubview:backgroundImageFill];

        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 50, 50)];
        [_imageView.layer setCornerRadius:2];
        [_imageView setClipsToBounds:YES];
        [self.contentView addSubview:_imageView];
    }

    return self;
}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 5;
    frame.size.width -= 10;
    [super setFrame:frame];
}
- (void)layoutSubviews {
    [super layoutSubviews];


    [self.textLabel setFrame:CGRectMake(55, 0, self.frame.size.width - 55, 50)];
    [self.textLabel setBackgroundColor:[UIColor clearColor]];
    [self.textLabel setFont:[UIFont boldSystemFontOfSize:17.0]];
    [self.textLabel setTextAlignment: NSTextAlignmentCenter];
    [self.textLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];


    [self.detailTextLabel setFrame:CGRectMake(55, 30, self.frame.size.width - 55, 15)];
    [self.detailTextLabel  setBackgroundColor:[UIColor clearColor]];
    [self.detailTextLabel  setFont:[UIFont systemFontOfSize:15.0]];
    [self.detailTextLabel  setTextAlignment: NSTextAlignmentCenter];


}

- (void)dealloc {
    [_imageView release];
    [super dealloc];
}


@end