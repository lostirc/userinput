//
// Created by frank on 10/7/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#include <UIKit/UIKit.h>
#include "UITextViewWithPlaceholder.h"

@protocol BottomTextInputSubmittedDelegate<NSObject>
    @optional -(void) bottomTextSubmitted:(NSString *) text;
@end


@interface BottomTextInput : UIView<UITextViewDelegate>{
    id<BottomTextInputSubmittedDelegate> delegate;
}


-(void) openBottomTextInput;
-(void) closeBottomTextInput;
-(void) allocBottomTextInput:(UIView * ) view;

@property(nonatomic, assign)id  delegate;
@property CGRect inputLocation;


@end