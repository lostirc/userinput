//
// Created by frank on 11/13/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "FavoritesViewCell.h"


@implementation FavoritesViewCell {

}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 5;
    frame.size.width -= 10;

    [super setFrame:frame];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(5, 5, 50, 50);

    self.detailTextLabel.frame = CGRectMake(57, 2, self.frame.size.width- 59, 45);
    [self.detailTextLabel setTextAlignment:NSTextAlignmentCenter];
    [self.detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.detailTextLabel setFont:[UIFont boldSystemFontOfSize:16.0]];
    [self.detailTextLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
    [self.detailTextLabel setNumberOfLines:0];

    self.textLabel.frame = CGRectMake(2, 52, self.frame.size.width-2, self.frame.size.height-54);
    [self.textLabel setTextAlignment:NSTextAlignmentCenter];
    [self.textLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.textLabel setFont:[UIFont systemFontOfSize:14.0]];
    [self.textLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
    [self.textLabel setNumberOfLines:0];
}

- (void)startActivityIndicator {
    self.activityIndicator = [[[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(15, 15, 15, 15)] autorelease];
    [self.activityIndicator setColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
    [self addSubview:self.activityIndicator];
    [self.activityIndicator startAnimating];

}

- (void)stopActivityIndicator {
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


@end