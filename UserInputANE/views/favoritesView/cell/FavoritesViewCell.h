//
// Created by frank on 11/13/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface FavoritesViewCell : UITableViewCell

@property (nonatomic, retain) UIActivityIndicatorView * activityIndicator;
-(void) startActivityIndicator;
-(void) stopActivityIndicator;
@end