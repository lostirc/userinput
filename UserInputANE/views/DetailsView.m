//
// Created by frank on 10/20/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "DetailsView.h"
#import "TopViewContainer.h"


@implementation DetailsView {
    UIView * parentView;
    QuestionAnswerDetailsView * qaDetailsView;
    UserDetailsView * userDetailsView;
    ShareView * shareView;
    InboxView * inboxView;
    TopViewContainer * topView;
    ViewOpenType openView;
}

@synthesize delegate;

- (void)checkIfDataLoadNeeded:(ViewOpenType)view {
    switch (view){
        case kUserView:
                if([[userDetailsView loadedData] objectForKey:@"friends"] == nil){
                    if([delegate respondsToSelector:@selector(dispatchDetailsDataRequest:)]){
                        [delegate dispatchDetailsDataRequest:kUserView];
                    }
                }
            break;
        case kDetailView:
            if([qaDetailsView checkForDataLoadNeeded]){
                if([delegate respondsToSelector:@selector(dispatchDetailsDataRequest:)]){
                    [delegate dispatchDetailsDataRequest:kDetailView];
                }
            }
            break;
        case kInboxView:
            if([inboxView loadedData] == nil){
                if([delegate respondsToSelector:@selector(dispatchDetailsDataRequest:)]){
                    [delegate dispatchDetailsDataRequest:kInboxView];
                }
            }
            break;
        case kShareView:
            if([shareView loadedData] == nil){
                if([delegate respondsToSelector:@selector(dispatchDetailsDataRequest:)]){
                    [delegate dispatchDetailsDataRequest:kShareView];
                }
            }
            break;

        case kTopView:
            if([topView checkForDataLoadNeeded]){
                if([delegate respondsToSelector:@selector(dispatchDetailsDataRequest:)]){
                    [delegate dispatchDetailsDataRequest:kTopView];
                }
            }
            break;
        case kReopenSameView:break;
    }
}

-(DetailsView *) withSuperview:(UIView *) superView withView: (ViewOpenType) viewType{
    parentView = superView;
    openView = viewType;
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];

    NSString * dragBarLoc = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"dragBar"] ofType:@".png" inDirectory:@"com/eyecu/icebreaker/mobile/assets"];
    UIImage * titleImage = [UIImage imageWithContentsOfFile:dragBarLoc];
    UIView * dragBar = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 30)] autorelease];
    [dragBar setBackgroundColor:[UIColor colorWithPatternImage:titleImage]];

    UILongPressGestureRecognizer * down = [[[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleTouchDown:)] autorelease];
    [down setMinimumPressDuration:.001];
    [dragBar addGestureRecognizer:down];
    [self addSubview:dragBar];


    switch (openView){
        case kUserView:
            [self setupUserView];
            break;
        case kDetailView:
            [self setupDetailsView];
            break;
        case kInboxView:
            [self setupInboxView];
            break;
        case kShareView:
            [self setupShareView];
            break;
        case kTopView:
            [self setupTopView];
            break;

        case kReopenSameView:break;
    }

}

-(void) handleTouchDown:(UILongPressGestureRecognizer *)recognizer{
    CGPoint location = [recognizer locationInView:parentView];
    CGRect newPos = self.frame;
    switch (recognizer.state) {
        case UIGestureRecognizerStateBegan:
            break;
        case UIGestureRecognizerStateChanged:
            if(location.y >= 105){
                newPos.origin.y = location.y;

                [UIView animateWithDuration:0.1 animations:^{
                    [self setFrame:newPos];
                }];
            }
            break;
        case UIGestureRecognizerStateEnded:

            if([delegate respondsToSelector:@selector(detailsViewDragEnded:)]){
                [delegate detailsViewDragEnded:location];
            }
            break;
        default:
            break;
    }


}

-(void) switchView:(ViewOpenType) view{
    switch (view){
        case kUserView:
            [self removeOpenView];
            [self addUserView];
            break;
        case kDetailView:
            [self removeOpenView];
            [self addDetailsView];
            break;
        case kInboxView:
            [self removeOpenView];
            [self addInboxView];
            break;
        case kShareView:
            [self removeOpenView];
            [self addShareView];
            break;
        case kTopView:
            [self removeOpenView];
            [self addTopView];
            break;
        case kReopenSameView:break;
    }
}

-(void) removeOpenView{
    switch (openView){

        case kDetailView:
            [qaDetailsView removeFromSuperview];
            break;
        case kUserView:
            [userDetailsView removeFromSuperview];
            break;
        case kInboxView:
            [inboxView removeFromSuperview];
            break;
        case kShareView:
            [shareView removeFromSuperview];
            break;
        case kTopView:
            [topView removeFromSuperview];
            break;
        case kReopenSameView:break;
    }
}

-(void) setupShareView{
    shareView = [[ShareView alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, self.frame.size.height) withParentView:parentView];
    [self addShareView];
    [shareView setupInterface];
}

-(void) addShareView{
    openView = kShareView;
    [self addSubview:shareView];
}

-(void) setupDetailsView{
    qaDetailsView = [[QuestionAnswerDetailsView alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, self.frame.size.height)];
    [self addDetailsView];
    [qaDetailsView setupInterface];

}

-(void) addDetailsView{
    openView = kDetailView;
    [self addSubview:qaDetailsView];
}

-(void) setupUserView{
    userDetailsView = [[UserDetailsView alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, self.frame.size.height) withParentView:parentView];
    [self addUserView];
    [userDetailsView setupInterface];

}

-(void) addUserView{
    if(userDetailsView == nil){
        [self setupUserView];
        [self checkIfDataLoadNeeded:kUserView];
    }
    openView = kUserView;
    [self addSubview:userDetailsView];
}

-(void) setupInboxView{
    inboxView = [[InboxView alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, self.frame.size.height)];
    [self addInboxView];
    [inboxView setupInterface];
}

-(void) addInboxView{
    if(inboxView == nil){
        [self setupInboxView];
        [self checkIfDataLoadNeeded:kInboxView];
    }
    openView = kInboxView;
    [self addSubview:inboxView];
}

-(void) setupTopView{
    topView = [[TopViewContainer alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, self.frame.size.height)];
    [self addTopView];
    [topView setupInterface];
}

-(void) addTopView{
    if(topView == nil){
        [self setupTopView];
        [self checkIfDataLoadNeeded:kTopView];
    }
    openView = kTopView;
    [self addSubview:topView];
}

-(void) setShareData:(NSString *)data responder: (NSObject *) responder {
    [shareView setTableData:data];
    shareView.delegate = responder;
}
- (void)setDetailsData:(NSString *)data responder: (NSObject *) responder {
    [qaDetailsView setTableData:data];
    qaDetailsView.delegate = responder;

}

- (void)setUserDetailsData:(NSString *)data responder: (NSObject *) responder {
    if(openView == kUserView){
        [userDetailsView setTableData:data];
        userDetailsView.delegate = responder;
    }
}

- (void) setInboxViewData:(NSString *)data responder: (NSObject *) responder{
    if(openView == kInboxView){
        [inboxView setTableData:data];
        inboxView.delegate = responder;
    }
}

- (void) setTopViewData:(NSString *)data responder: (NSObject *) responder{
    if(openView == kTopView){
        [topView setTableData:data withResponder:responder];
        topView.delegate = responder;
    }
}

- (void)dealloc {
    [qaDetailsView release];
    [userDetailsView release];
    [shareView release];
    [inboxView release];
    [topView release];
    [super dealloc];
}

@end