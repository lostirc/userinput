//
// Created by frank on 11/8/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "DetailsTableViewCell.h"
#import "AnswerView.h"
#import "QuestionView.h"
#import "FavoritesView.h"

//todo: move this out, this is wrong!
@protocol QuestionAnswerDetailsViewDelegate<NSObject>
    @optional
    -(void) detailsItemSelected:(NSString *) item;
    -(void) detailsMultiItemSelected:(NSDictionary *) item;
@end

@interface QuestionAnswerDetailsView : UIView<DetailsItemSelectedDelegate>

- (BOOL)checkForDataLoadNeeded;

- (void)setTableData: (NSString *) tableJson;
- (void)setupInterface;

@property (nonatomic, assign)id delegate;
@end