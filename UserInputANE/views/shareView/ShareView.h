//
// Created by frank on 11/21/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FriendGridCell.h"
#import "vo/SelectedFriend.h"

#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@protocol ShareWithFriendDelegate<NSObject>
@optional
    -(void) shareWithFriends:(SelectedFriend *) selectedFriend;
@end

@interface ShareView : UIView <UISearchBarDelegate, UIGestureRecognizerDelegate, UICollectionViewDelegate, UICollectionViewDataSource>{
    id<ShareWithFriendDelegate> delegate;
}

@property (nonatomic, assign)id delegate;
@property (nonatomic, strong) NSDictionary * loadedData;
@property (nonatomic, strong) NSMutableArray * icebreakerFriendsFilter;
@property (nonatomic, strong) NSMutableArray *allFriendsFilter;

- (id)initWithFrame:(CGRect)frame withParentView:(UIView *)parentView;

- (void)setupInterface;
- (void)setTableData: (NSString *) data;
@end