//
// Created by frank on 11/22/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>


typedef enum {
    kIcebreakerFriend,
    kFacebookFriend
} FriendType;

@interface SelectedFriend : NSObject

@property (nonatomic, strong) NSString * friendName;
@property (nonatomic, strong) NSString * friendId;
@property (nonatomic, strong) NSURL * imageUrl;
@property (nonatomic) FriendType  friendType;


- (id)initWithName:(NSString *)name withFriendId:(NSString *)friendId withImageUrl:(NSURL *)imageUrl withFriendType:(FriendType)type;
@end