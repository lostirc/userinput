//
// Created by frank on 11/22/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//



#import "SelectedFriend.h"


@implementation SelectedFriend {

}

- (id)initWithName: (NSString *) name  withFriendId:(NSString *) friendId withImageUrl:(NSURL *) imageUrl withFriendType:(FriendType) type{
    self = [super init];
    if (self) {
        [self setFriendName:name];
        [self setFriendId:friendId];
        [self setImageUrl:imageUrl];
        [self setFriendType:type];
    }

    return self;
}

@end