//
// Created by frank on 11/21/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "FriendGridCell.h"


@implementation FriendGridCell {

}

@synthesize imageView = _imageView;
@synthesize nameLabel = _nameLabel;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {

        NSString * backgroundLoc = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"viewBackground"]
                                                                   ofType:@".png"
                                                              inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

        UIImage * backgroundImage = [[UIImage imageWithContentsOfFile:backgroundLoc] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                                    resizingMode:UIImageResizingModeTile];


        UIView * backgroundImageFill = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 157, 54)] autorelease];
        [backgroundImageFill setTag:1];
        [backgroundImageFill.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [backgroundImageFill.layer setBorderWidth:1];
        [backgroundImageFill.layer setCornerRadius:2];
        [backgroundImageFill setClipsToBounds:YES];
        [backgroundImageFill setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];

        [self.contentView addSubview:backgroundImageFill];

        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 50, 50)];
        [_imageView.layer setCornerRadius:2];
        [_imageView setClipsToBounds:YES];
        [self.contentView addSubview:_imageView];

        _nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(54, 0, 100, 54)];
        [_nameLabel setTextAlignment:NSTextAlignmentCenter];
        [_nameLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_nameLabel setFont:[UIFont boldSystemFontOfSize:15.0]];
        [_nameLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
        [_nameLabel setNumberOfLines:0];
        [self.contentView addSubview:_nameLabel];
    }

    return self;
}

-(void) setSelected:(BOOL) selected{
    [super setSelected:selected];
    if(selected){
        [UIView animateWithDuration:0.3 animations:^{
            [self setAlpha:0.5];
        }];

    } else {
        [UIView animateWithDuration:0.3 animations:^{
            [self setAlpha:1];
        }];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];

}


- (void)prepareForReuse {
    [super prepareForReuse];
//    self.selected = NO;

    self.imageView.image = nil;

    self.imageView.frame = CGRectMake(2, 2, 50, 50);
}



- (void)dealloc {
    [_imageView release];
    [_nameLabel release];
    [super dealloc];
}


@end