//
// Created by frank on 11/21/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "ShareView.h"


@implementation ShareView {
    UITapGestureRecognizer * closeTextInputRecognizer;
    UIView * rootView;
    UISearchBar * searchBar;
    UICollectionView * friendsTable;
    BOOL inAllFriendsView;
    BOOL searchIsOpen;

}

@synthesize delegate;


- (id)initWithFrame:(CGRect)frame withParentView:(UIView *) parentView {
    self = [super initWithFrame:frame];
    if (self) {
        rootView = parentView;
    }

    return self;
}




- (void)setupInterface {

    UICollectionViewFlowLayout * collectionLayout = [[[UICollectionViewFlowLayout alloc] init] autorelease];
    [collectionLayout setItemSize:CGSizeMake(157, 54)];
    [collectionLayout setSectionInset:UIEdgeInsetsMake(2, 2, 2, 2)];
    [collectionLayout setMinimumInteritemSpacing:2];
    [collectionLayout setMinimumLineSpacing:2];


    friendsTable = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 70, self.frame.size.width, self.frame.size.height - 100) collectionViewLayout:collectionLayout];

    [friendsTable registerClass:[FriendGridCell class] forCellWithReuseIdentifier:@"imageCell"];
    [friendsTable setAllowsMultipleSelection:YES];

    [friendsTable setBackgroundColor:[[[UIColor alloc] initWithWhite:1 alpha:0.0] autorelease]];
    friendsTable.delegate = self;

    [friendsTable setDataSource:self];



    [self addSubview:friendsTable];

    UIView * headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 68)] autorelease];
    [headerView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [headerView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [headerView.layer setShadowOffset:CGSizeMake(0, 5)];
    [headerView.layer setShadowOpacity:0.5];

    [self addSubview:headerView];



    UISegmentedControl * gridControl = [[[UISegmentedControl alloc] initWithItems:[NSArray arrayWithObjects:@"Icebreaker Friends", @"All Friends", nil]] autorelease];
    [gridControl addTarget:self action:@selector(handleSwitchData:) forControlEvents:UIControlEventValueChanged];
    [gridControl setFrame:CGRectMake(2, 2, self.frame.size.width - 4, 30)];
    [gridControl setTintColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];


    [gridControl setSelectedSegmentIndex:0];
    [headerView addSubview:gridControl];

    searchBar = [[[UISearchBar alloc] initWithFrame:CGRectMake(0, 34, self.frame.size.width, 30)] autorelease];
    searchBar.delegate = self;
    searchBar.translucent = YES;
    searchBar.backgroundImage = [[UIImage new] autorelease];
    searchBar.scopeBarBackgroundImage = [[UIImage new] autorelease];
    [headerView addSubview:searchBar];


}



-(void) handleSwitchData: (id) sender{
    if(searchIsOpen){
        [self searchEndEditing];
        self.icebreakerFriendsFilter = [self.loadedData valueForKey:@"icebreakerFriends"];
        self.allFriendsFilter = [self.loadedData valueForKey:@"facebookFriends"];
        [searchBar setText:@""];

    }
    UISegmentedControl * control = sender;
    inAllFriendsView = [control selectedSegmentIndex] == 1;
    [friendsTable reloadData];


}

- (void)setTableData:(NSString *)data {
    NSError * e;
    self.loadedData = [NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding]
                                                      options:0
                                                        error:&e];

    self.icebreakerFriendsFilter = [self.loadedData valueForKey:@"icebreakerFriends"];
    self.allFriendsFilter = [self.loadedData valueForKey:@"facebookFriends"];

    [friendsTable reloadData];

}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)bar {
    searchIsOpen = YES;
    closeTextInputRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(searchEndEditing)] autorelease];
    closeTextInputRecognizer.delegate = self;
    [rootView addGestureRecognizer:closeTextInputRecognizer];
    return YES;
}

-(void) searchEndEditing{
    searchIsOpen = NO;
    closeTextInputRecognizer.delegate = nil;
    [rootView removeGestureRecognizer:closeTextInputRecognizer];
    [searchBar resignFirstResponder];
}

- (void)searchBar:(UISearchBar *)searchBar1 textDidChange:(NSString *)searchText {
    if(searchText.length == 0) {

        self.allFriendsFilter = [self.loadedData valueForKey:@"facebookFriends"];
        self.icebreakerFriendsFilter = [self.loadedData valueForKey:@"icebreakerFriends"];

    } else {

        if(inAllFriendsView){
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",searchText];
            self.allFriendsFilter = [[[self.loadedData valueForKey:@"facebookFriends"] filteredArrayUsingPredicate:predicate] mutableCopy];

        } else {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@",searchText];
            self.icebreakerFriendsFilter = [[[self.loadedData valueForKey:@"icebreakerFriends"] filteredArrayUsingPredicate:predicate] mutableCopy];
        }

    }

    [friendsTable reloadData];
}


- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if(inAllFriendsView){
        return [self.allFriendsFilter count];
    } else {
        return [self.icebreakerFriendsFilter count];
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"imageCell";

    FriendGridCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];

    NSString * imgUrl;
    NSString * name;
    if(inAllFriendsView){
        imgUrl = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=100&height=100", [[self.allFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"username"]];
        name = [[self.allFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"name"];
    } else {
        imgUrl = [NSString stringWithFormat:@"%@?width=100&height=100", [[self.icebreakerFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"photo"]];
        name = [[self.icebreakerFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"name"];
    }

    NSLog(@"SHARE VIEW, imgUrl: %@, name: %@", imgUrl, name);

    if(![imgUrl isKindOfClass:[NSNull class]]){
        [cell.imageView setImageWithURL:[NSURL URLWithString:imgUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    }
    if(![name isKindOfClass:[NSNull class]]){
        [cell.nameLabel setText:name];
    }

    if(cell.selected){
        [cell setSelected:YES];
    } else {
        [cell setSelected:NO];
    }

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    FriendGridCell *datasetCell = (FriendGridCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [self inviteFriend:[self makeSelectedFriend:indexPath]];
    [datasetCell setSelected:YES];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    FriendGridCell *datasetCell = (FriendGridCell *) [collectionView cellForItemAtIndexPath:indexPath];
    [datasetCell setSelected:NO];
}


- (void)dealloc {
    [friendsTable release];
    [self.allFriendsFilter release];
    [self.icebreakerFriendsFilter release];
    [super dealloc];
}

-(SelectedFriend *) makeSelectedFriend:(NSIndexPath *)indexPath{
    NSString * name;
    NSString * id;
    NSURL * imageUrl;
    FriendType friendType;


    if(inAllFriendsView){
        imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=100&height=100", [[self.allFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"username"]]];
        name = [[self.allFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"name"];
        id = [NSString stringWithFormat:@"%@",
        [[self.allFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"uid"]];
        friendType = kFacebookFriend;
    } else {

        imageUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%@?width=100&height=100", [[self.icebreakerFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"photo"]]];
        name = [[self.icebreakerFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"name"];
        id = [[self.icebreakerFriendsFilter objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"fbUserId"];
        friendType = kIcebreakerFriend;

    }

    return [[[SelectedFriend alloc] initWithName:name withFriendId:id withImageUrl:imageUrl withFriendType:friendType] autorelease];
}

-(void) inviteFriend:(SelectedFriend *) friend{
    if([delegate respondsToSelector:@selector(shareWithFriends:)]){
        [delegate shareWithFriends:friend];
    }
}

@end