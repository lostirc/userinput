//
// Created by frank on 11/18/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AnswerViewCell.h"
#import "DetailsItemSelectedDelegate.h"
#import "SizeUtil.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"


@interface AnswerView : UIView <UITableViewDelegate, UITableViewDataSource>{
    id<DetailsItemSelectedDelegate> delegate;
}

//@property (nonatomic, strong) NSMutableArray * imgCache;
@property (nonatomic, assign)id delegate;
@property (nonatomic, strong) NSArray * loadedData;

- (NSString *)getReuseIdentifier;

- (void)setupInterface;
- (void)setTableData: (NSArray *) data;

@end