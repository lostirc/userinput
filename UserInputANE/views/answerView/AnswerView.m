//
// Created by frank on 11/18/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "AnswerView.h"



@implementation AnswerView {
    UITableView * answerTable;
}

@synthesize delegate;


-(NSString *) getReuseIdentifier{
    return @"answerViewCell";
}

- (void)setupInterface{
    answerTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)] autorelease];
    [answerTable setBackgroundColor:[[[UIColor alloc] initWithWhite:1 alpha:0.0] autorelease]];
    [answerTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [answerTable registerClass:[AnswerViewCell class] forCellReuseIdentifier:[self getReuseIdentifier]];
    answerTable.delegate = self;
    [answerTable setDataSource:self];
    [answerTable setTableHeaderView:[[[UIView alloc] initWithFrame:CGRectMake(0, 0, answerTable.frame.size.width, 40)] autorelease]];
    [self addSubview:answerTable];
}

- (void)setTableData:(NSArray *)data {
    self.loadedData = data;
    [answerTable reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.loadedData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AnswerViewCell * cell = [tableView dequeueReusableCellWithIdentifier:[self getReuseIdentifier]];


    NSString * ansImgUrl = [NSString stringWithFormat:@"%@?width=100&height=100",
                                        [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"answer.user.photo"]];
    [cell.imageView setImageWithURL:[NSURL URLWithString:ansImgUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    //set details

    if([[self getReuseIdentifier] isEqualToString:@"answerViewCell"]){
        [cell.detailTextLabel setText:[NSString stringWithFormat:@"Answer Score (%@)", [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"answer.answerScore"]]];
    } else {
        [cell.detailTextLabel setText:[NSString stringWithFormat:@"%@ (%@)\nAnswer Score (%@)",
             [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"answer.user.name"],
             [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"answer.user.score"],
             [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"answer.answerScore"]]];
    }


    NSString * question = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"question.question"];
    [cell setQuestionText:question];


    [cell setAnswerText:[[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"answer.answer"]];


    BOOL lock = [[[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"question.closed"] boolValue];
    [cell lockQuestion:lock];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGSize maxSize = CGSizeMake(answerTable.frame.size.width - 14, FLT_MAX);
    NSString * question = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"question.question"];

    NSString * answer = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"answer.answer"];
    return ([SizeUtil text:answer heightWithFont:[UIFont systemFontOfSize:14]constrainedToSize:maxSize] + 60) +
            ([SizeUtil text:question heightWithFont:[UIFont systemFontOfSize:11]constrainedToSize:maxSize] + 9);

}




- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString * answer = [[self.loadedData objectAtIndex:(NSUInteger) (indexPath.row)] valueForKeyPath:@"answer.id"];
    NSString * question = [[self.loadedData objectAtIndex:(NSUInteger) (indexPath.row)] valueForKeyPath:@"question.id"];

    NSArray * keys = @[@"question", @"answer"];
    NSArray * values = @[question, answer];

    NSDictionary * dictionary = [[[NSDictionary alloc] initWithObjects:values forKeys:keys] autorelease];
    if([delegate respondsToSelector:@selector(detailsMultiValueItemSelected:)]){
        [delegate detailsMultiValueItemSelected:dictionary];
    }

}

- (void)dealloc {
    [super dealloc];
}


@end