//
// Created by frank on 11/18/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "AnswerViewCell.h"
#import "SizeUtil.h"


@implementation AnswerViewCell {

}


@synthesize answerView = _answerView;


@synthesize answerLabel = _answerLabel;


- (CGFloat)questionLabelFontSize {
    return 11;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {
        NSString * backgroundLoc = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"viewBackground"]
                                                                   ofType:@".png"
                                                              inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

        UIImage * backgroundImage = [[UIImage imageWithContentsOfFile:backgroundLoc] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                                    resizingMode:UIImageResizingModeTile];



        _answerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 10)];

        [_answerView.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [_answerView.layer setBorderWidth:1];
        [_answerView.layer setCornerRadius:2];
        [_answerView setClipsToBounds:YES];
        [_answerView setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];
        [self.contentView addSubview:_answerView];


        _answerLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, self.frame.size.width-4, 0)];
        [_answerLabel setTextAlignment:NSTextAlignmentCenter];
        [_answerLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_answerLabel setFont:[UIFont systemFontOfSize:14.0]];
        [_answerLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
        [_answerLabel setNumberOfLines:0];
        [_answerView addSubview:_answerLabel];


    }

    return self;
}


- (void)setQuestionText:(NSString *)question {
    [super setQuestionText:question];
    [_answerView setFrame:CGRectMake(0, self.questionLabel.frame.origin.y + self.questionLabel.frame.size.height + 5, self.questionLabel.frame.size.width + 4, 54)];
}

- (void)setAnswerText:(NSString *)answer {
    [_answerLabel setText:answer];

    CGSize maximumAnswerLabelSize = CGSizeMake(_answerLabel.frame.size.width, FLT_MAX);

    CGRect newViewFrame = _answerLabel.frame;
    newViewFrame.size.height = [SizeUtil text:answer heightWithFont:_answerLabel.font constrainedToSize:maximumAnswerLabelSize];


    [_answerLabel setFrame:newViewFrame];

    newViewFrame = _answerView.frame;

    newViewFrame.size.height = _answerLabel.frame.size.height + _answerLabel.frame.origin.y + 5;

    [_answerView setFrame:newViewFrame];

}


- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)dealloc {
    [_answerView release];
    [_answerLabel release];
    [super dealloc];
}

@end