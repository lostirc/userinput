//
// Created by frank on 11/18/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "DetailsTableViewCell.h"


@interface AnswerViewCell : DetailsTableViewCell

@property (nonatomic, strong) UIView * answerView;
@property (nonatomic, strong) UILabel * answerLabel;


- (void)setAnswerText:(NSString *)answer;
@end