//
// Created by frank on 11/11/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FindFriendTableViewCell.h"
#import "UIButtonWithData.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"

@protocol FriendInvitedDelegate<NSObject>
@optional
    -(void)friendInvited:(NSString *) friendId;
@end

@interface FindFriendDropdown : UITableView<UITableViewDelegate, UITableViewDataSource>{
    id<FriendInvitedDelegate> friendSelectedDelegate;
}

@property (nonatomic, assign)id friendSelectedDelegate;

@property (nonatomic, strong) NSArray * loadedData;
//@property (nonatomic, strong) NSMutableDictionary * imgCache;

-(void) setTableData:(NSMutableArray *)data;




@end