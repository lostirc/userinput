//
// Created by frank on 11/9/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "UserDetailsView.h"



@implementation UserDetailsView {
    UITableView * usersTable;
}

@synthesize delegate;

- (id)initWithFrame:(CGRect)frame withParentView:(UIView *) parentView {
    self = [super initWithFrame:frame];
    if (self) {
    }

    return self;
}


- (void)setupInterface {
    usersTable = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, self.frame.size.width, self.frame.size.height - 60)];
    [usersTable setBackgroundColor:[[[UIColor alloc] initWithWhite:1 alpha:0.0] autorelease]];
    [usersTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [usersTable registerClass:[UserDetailsTableViewCell class] forCellReuseIdentifier:@"userDetailsTableViewCell"];
    [usersTable setTableHeaderView:[[[UIView alloc] initWithFrame:CGRectMake(0, 0, usersTable.frame.size.width, 10)] autorelease]];

    usersTable.delegate = self;

    [usersTable setDataSource:self];

    [self addSubview:usersTable];

    UIView * headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 34)] autorelease];
    [headerView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [headerView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [headerView.layer setShadowOffset:CGSizeMake(0, 5)];
    [headerView.layer setShadowOpacity:0.5];


    NSString * fbLogo = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"fb_25px"]
                                                               ofType:@".png"
                                                          inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

    UIImage * fbLogoImage = [[UIImage imageWithContentsOfFile:fbLogo] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                                resizingMode:UIImageResizingModeTile];


    UIButton * inviteFriendsButton = [[[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 34)] autorelease];
    [inviteFriendsButton setTitleColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f] forState:UIControlStateNormal];
    [inviteFriendsButton setTitle:@"Invite Friends" forState:UIControlStateNormal];
    [inviteFriendsButton addTarget:self action:@selector(inviteFriendButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
    [inviteFriendsButton setImage:fbLogoImage forState:UIControlStateNormal];

    inviteFriendsButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
    inviteFriendsButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);

    [headerView addSubview:inviteFriendsButton];
    [self addSubview:headerView];


}

-(void) inviteFriendButtonHandler:(id) sender{

    [[FBSession activeSession] closeAndClearTokenInformation];
    NSArray* expectedPermissions = @[@"read_stream"];
    NSString * fbSessionId = [self.loadedData valueForKey:@"currentUserSessionId"];

    NSDictionary *dictionary = @{
            FBTokenInformationTokenKey:fbSessionId,
            FBTokenInformationExpirationDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
            FBTokenInformationPermissionsKey:expectedPermissions,
            FBTokenInformationLoginTypeLoginKey:[NSNumber numberWithInt:FBSessionLoginTypeFacebookViaSafari],
            FBTokenInformationRefreshDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
            FBTokenInformationPermissionsRefreshDateKey:[NSDate dateWithTimeIntervalSince1970:9999999999],
    };

    FBAccessTokenData * tokenData = [FBAccessTokenData createTokenFromDictionary:dictionary];

    FBSession * session = [[[FBSession alloc] init] autorelease];

    [session openFromAccessTokenData:tokenData completionHandler:^(FBSession *completedSession, FBSessionState sessionState, NSError * sessionError){


        [FBWebDialogs presentRequestsDialogModallyWithSession:completedSession
                                                      message:@"Check out Icebreaker, a social game of questions and answers."
                                                        title:@"Join me on Icebreaker"
                                                   parameters:nil
                                                      handler:nil];
    }];

}




//table
- (void)setTableData:(NSString *)tableJson {
    NSError * e;
    self.loadedData = [NSJSONSerialization JSONObjectWithData:[tableJson dataUsingEncoding:NSUTF8StringEncoding]
                                                      options:0
                                                        error:&e];

    [usersTable reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self.loadedData valueForKey:@"friends"] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellId = @"userDetailsTableViewCell";
    UserDetailsTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];

    NSString * imageUrl =[NSString stringWithFormat:@"%@?width=100&height=100", [[[self.loadedData valueForKey:@"friends"] objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"photo"]];
    [cell.imageView setImageWithURL:[NSURL URLWithString:imageUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];



    NSString * facebookName = [[[self.loadedData valueForKey:@"friends"] objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"name"];
    NSString * score = [[[self.loadedData valueForKey:@"friends"] objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"score"];

    [cell.textLabel setText:[NSString stringWithFormat:@"%@ (%@)", facebookName, score]];

    //todo: add something under the username, not sure what yet

//    NSString * totals = [NSString stringWithFormat:@"questions: %@ answers: %@",
//                    [[[self.loadedData valueForKey:@"friends"] objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"questionsTotal"],
//                    [[[self.loadedData valueForKey:@"friends"] objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"answersTotal"]];
//
//
//    [cell.detailTextLabel setText:totals];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 57;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSError * e;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:[[self.loadedData valueForKey:@"friends"] objectAtIndex:(NSUInteger) indexPath.row] options:NSJSONWritingPrettyPrinted error:&e];
    NSString * jsonString = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
    if([delegate respondsToSelector:@selector(userItemSelected:)]){
        [delegate userItemSelected:jsonString];
    }
}


- (void)dealloc {
    [usersTable release];
    [super dealloc];
}


@end