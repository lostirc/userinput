//
// Created by frank on 11/9/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>
#import "DetailsTableViewCell.h"
#import "FindFriendDropdown.h"
#import "UserDetailsTableViewCell.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <FacebookSDK/FacebookSDK.h>

@protocol UserDetailsViewDelegate<NSObject>
    @optional
    -(void) userItemSelected:(NSString *) item;
@end

@interface UserDetailsView : UIView<FriendInvitedDelegate, UITableViewDelegate, UITableViewDataSource> {
    id<UserDetailsViewDelegate> delegate;
}


- (void)setTableData: (NSString *) tableJson;

- (id)initWithFrame:(CGRect)frame withParentView:(UIView *)parentView;

- (void)setupInterface;


@property (nonatomic, assign)id delegate;
@property (nonatomic, strong) NSDictionary * loadedData;
@end