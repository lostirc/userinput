//
// Created by frank on 11/12/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface FindFriendTableViewCell : UITableViewCell

@property (nonatomic, retain) NSString * imageSource;
@property (nonatomic, strong) UIImageView * imageView;

@end