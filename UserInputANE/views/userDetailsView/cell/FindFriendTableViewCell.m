//
// Created by frank on 11/12/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "FindFriendTableViewCell.h"


@implementation FindFriendTableViewCell {

}

@synthesize imageView = _imageView;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {

        [self setBackgroundColor:[UIColor clearColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setIndentationWidth:0.0];

        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 25, 25)];
        [_imageView.layer setCornerRadius:2];
        [_imageView setClipsToBounds:YES];
        [self.contentView addSubview:_imageView];
    }

    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self.textLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
    self.textLabel.frame = CGRectMake(35, 0, self.frame.size.width - 30, 30);

}

- (void)dealloc {
    [_imageView release];
    [super dealloc];
}

@end