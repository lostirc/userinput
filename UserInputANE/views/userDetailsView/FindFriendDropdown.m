//
// Created by frank on 11/11/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "FindFriendDropdown.h"



@implementation FindFriendDropdown {
    UILabel * noFriendsLabel;
}

@synthesize friendSelectedDelegate;

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        [self setDataSource:self];
        [self.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [self.layer setBorderWidth:1];
        [self.layer setCornerRadius:3];
        [self setClipsToBounds:YES];
        [self setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:0.9f]];
        [self setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        noFriendsLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width - 10, 30)] autorelease];
        [noFriendsLabel setFont:[UIFont italicSystemFontOfSize:16]];
        [noFriendsLabel setText:@"No friends found...."];
        [noFriendsLabel setTextAlignment: NSTextAlignmentCenter];
        [noFriendsLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
        [self setTableHeaderView:noFriendsLabel];

    }

    return self;
}



-(void) setTableData:(NSMutableArray *)data{

    if(!self) return;

    self.loadedData = data;

    if([self.loadedData count] == 0){
        noFriendsLabel = [[[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.frame.size.width - 10, 30)] autorelease];
        [noFriendsLabel setFont:[UIFont italicSystemFontOfSize:16]];
        [noFriendsLabel setText:@"No friends found...."];
        [noFriendsLabel setTextAlignment: NSTextAlignmentCenter];
        [noFriendsLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
        [self setTableHeaderView:noFriendsLabel];
    } else {
        [self setTableHeaderView:nil];
    }

    [self reloadData];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if([self.loadedData count] * 30 < 90){
        CGRect frame = self.frame;
        if([self.loadedData count] == 0){
            frame.size.height =  30;
        } else {
            frame.size.height = [self.loadedData count] * 30;
        }

        [UIView animateWithDuration:0.2 animations:^{
            [self setFrame:frame];
        }];

    } else if (self.frame.size.height != 90) {
        CGRect frame = self.frame;
        frame.size.height = 90;
        [UIView animateWithDuration:0.2 animations:^{
            [self setFrame:frame];
        }];
    }
    return [self.loadedData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * cellId = @"facebookFriendCell";
    FindFriendTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if(cell == nil){
        cell = [[[FindFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId] autorelease];

        UIButtonWithData * addFriendButton = [[[UIButtonWithData alloc] init] autorelease];
        [addFriendButton setFrame:CGRectMake(self.frame.size.width - 65, 0, 60, 30)];
        [addFriendButton setTag:1];
        [addFriendButton setTitleColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f] forState:UIControlStateNormal];
        [addFriendButton setTitle:@"Invite" forState:UIControlStateNormal];
        [addFriendButton addTarget:self action:@selector(inviteFriendButtonHandler:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:addFriendButton];
    }

    [cell.textLabel setText:[[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"name"]];


    NSString * userName = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"username"];
    [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?width=50&height=50", userName]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];


    UIButtonWithData * invite = (UIButtonWithData *) [cell.contentView viewWithTag:1];
    [invite setData: [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"uid"]];

    return cell;
}

-(void) inviteFriendButtonHandler:(id) sender{
    UIButtonWithData * btn = (UIButtonWithData *) sender;
    NSLog(@"btn data: %@", [btn data]);
    if([friendSelectedDelegate respondsToSelector:@selector(friendInvited:)]){
        [friendSelectedDelegate friendInvited:[btn data]];

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (void)dealloc {
    [super dealloc];
}


@end