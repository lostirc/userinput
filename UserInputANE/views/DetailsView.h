//
// Created by frank on 10/20/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "QuestionAnswerDetailsView.h"
#import "UserDetailsView.h"
#import "ShareView.h"
#import "InboxView.h"
#import "TopView.h"

typedef enum{
    kDetailView,
    kUserView,
    kInboxView,
    kShareView,
    kTopView,
    kReopenSameView
} ViewOpenType;

@protocol DetailsViewDelegate<NSObject>
@optional
    -(void) detailsViewDragEnded:(CGPoint) location;
    -(void) dispatchDetailsDataRequest:(ViewOpenType) viewOpenType;

@end

@interface DetailsView : UIView  {
    id<DetailsViewDelegate> delegate;
}


@property (nonatomic, assign)id delegate;

- (DetailsView *)withSuperview: (UIView *) superView withView: (ViewOpenType) viewType;

- (void)setShareData:(NSString *)data responder:(NSObject *)responder;

- (void)setDetailsData:(NSString *)data responder: (NSObject *) responder;

- (void)setUserDetailsData:(NSString *)data responder: (NSObject *) responder;

- (void)setInboxViewData:(NSString *)data responder:(NSObject *)responder;

- (void)setTopViewData:(NSString *)data responder:(NSObject *)responder;

- (void)checkIfDataLoadNeeded: (ViewOpenType) view;
- (void)switchView: (ViewOpenType) view;


@end