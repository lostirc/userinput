//
// Created by frank on 10/11/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import "TopTextInput.h"




@implementation TopTextInput {
    UITextViewWithPlaceholder * input;
    UIView * inputView;
    UIView * parentView;
    UITapGestureRecognizer * closeTextInputRecognizer;
//    UILabel * questionCount;
}



@synthesize delegate;

- (void)openTopTextInput {
    if([[parentView gestureRecognizers] containsObject:closeTextInputRecognizer]){
        NSLog(@"we are already open");
    }
    closeTextInputRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeTopTextInput)] autorelease];
    [parentView addGestureRecognizer:closeTextInputRecognizer];
    [input becomeFirstResponder];
    [self doAnimation:YES];
}

- (void)closeTopTextInput {
    [parentView removeGestureRecognizer:closeTextInputRecognizer];
    [input setText:@""];
    [input resignFirstResponder];
    [self doAnimation:NO];


}

- (void)allocTopTextInput:(UIView *)view {

    parentView = view;

    input = [[[UITextViewWithPlaceholder alloc] initWithFrame:CGRectMake(2, 2, view.bounds.size.width - 4, 35)] autorelease];

    [input.layer setCornerRadius:2];
    [input setBackgroundColor:[UIColor whiteColor]];
    [input setKeyboardType:UIKeyboardTypeAlphabet];
    [input setReturnKeyType:UIReturnKeySend];
    [input setPlaceholder:@"Ask a question?"];
    [input setFont:[UIFont systemFontOfSize:15]];
    [input setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];

    [input layoutIfNeeded];

    input.delegate = self;


    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];

    CGRect startFrame = CGRectMake(view.bounds.size.width, 98, view.bounds.size.width, 39);


    inputView = [[[UIView alloc] initWithFrame:startFrame] autorelease];
    [inputView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];

    [inputView addSubview:input];

    [view addSubview:inputView];


}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [inputView removeFromSuperview];
    [super dealloc];
}


- (void)textViewDidChange:(UITextView *)textView {
    CGRect inputFrame = input.frame;
    __block CGFloat newHeight = [self textViewHeightForAttributedText:[input attributedText] andWidth:inputFrame.size.width];
    inputFrame.size.height = newHeight;

    __block CGRect inputViewFrame = inputView.frame;
    inputViewFrame.size.height = newHeight + 4;

    [UIView animateWithDuration:.2 animations:^{
        input.frame = inputFrame;

        inputView.frame = inputViewFrame;
    }];

}

- (CGFloat)textViewHeightForAttributedText: (NSAttributedString*)text andWidth: (CGFloat)width {
    UITextView *calculationView = [[[UITextView alloc] init] autorelease];
    [calculationView setAttributedText:text];
    CGSize size = [calculationView sizeThatFits:CGSizeMake(width, FLT_MAX)];
    return size.height > 35 ? size.height : 35;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {

    if([text isEqualToString:@"\n"]){
        [self doSubmit];
        return NO;
    }

    NSInteger newLength = [textView.text length] + [text length] - range.length;
    return newLength <= 200;
}



-(void) doSubmit{

    if([delegate respondsToSelector:@selector(topTextSubmitted:)]){
        [delegate topTextSubmitted:input.text];
    }

}


- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {

    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {

    return YES;
}

-(void)keyboardWillShow:(NSNotification *) notification{
    NSLog(@"Top Text Input keyboard will show");

}

-(void)keyboardWillHide:(NSNotification *)notification {
    NSLog(@"Top Text Input keyboard will hide");

}


-(void)doAnimation:(BOOL) open {

   __block CGRect offsetFrame;

    offsetFrame = inputView.frame;
    [UIView animateWithDuration:0.3 animations:^{
        offsetFrame.origin.x -= parentView.bounds.size.width;
        inputView.frame = offsetFrame;
    } completion:^(BOOL finished){
        if(!open){
            offsetFrame = CGRectMake(parentView.bounds.size.width, 98, parentView.bounds.size.width, 39);
            inputView.frame = offsetFrame;
            offsetFrame = CGRectMake(2, 2, input.bounds.size.width, 35);
            input.frame = offsetFrame;
            if([delegate respondsToSelector:@selector(topTextClosed)]){
                [delegate topTextClosed];
            }

        }
    }];

}



@end