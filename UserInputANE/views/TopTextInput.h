//
// Created by frank on 10/11/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//
// To change the template use AppCode | Preferences | File Templates.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UITextViewWithPlaceholder.h"

@protocol TopTextInputSubmittedDelegate<NSObject>
    @optional -(void) topTextSubmitted:(NSString *) text;
    @optional -(void) topTextClosed;
@end

@interface TopTextInput : UIView<UITextViewDelegate> {
    id<TopTextInputSubmittedDelegate> delegate;
}

-(void) openTopTextInput;
-(void) closeTopTextInput;
-(void) allocTopTextInput:(UIView * ) view;
//-(void) deallocTopTextInput;


@property (nonatomic, assign)id delegate;


@end