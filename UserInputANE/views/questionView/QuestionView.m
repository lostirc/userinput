//
// Created by frank on 11/18/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "QuestionView.h"




@implementation QuestionView {
    UITableView * questionTable;
}

@synthesize delegate;

-(NSString *) getReuseIdentifier{
    return @"currentUserQuestionsDetailCell";
}

- (void)setupInterface {
    questionTable = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)] autorelease];
    [questionTable setBackgroundColor:[[[UIColor alloc] initWithWhite:1 alpha:0.0] autorelease]];
    [questionTable setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [questionTable registerClass:[DetailsTableViewCell class] forCellReuseIdentifier:[self getReuseIdentifier]];
    questionTable.delegate = self;
    [questionTable setDataSource:self];
    [questionTable setTableHeaderView:[[[UIView alloc] initWithFrame:CGRectMake(0, 0, questionTable.frame.size.width, 40)] autorelease]];
    [self addSubview:questionTable];
}

- (void)setTableData:(NSArray *)data {
    self.loadedData = data;
    [questionTable reloadData];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.loadedData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    DetailsTableViewCell * cell = [questionTable dequeueReusableCellWithIdentifier:[self getReuseIdentifier]];



    NSString *imgUrl = [NSString stringWithFormat:@"%@?width=100&height=100",
                                                  [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"user.photo"]];

    [cell.imageView setImageWithURL:[NSURL URLWithString:imgUrl] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

    //set details
    //remove user name from current users view
    NSString * detailsText;
    if([[self getReuseIdentifier] isEqualToString:@"currentUserQuestionsDetailCell"]){
        detailsText = [NSString stringWithFormat:@"Question Score (%@)", [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"score"]];
    } else {
        detailsText = [NSString stringWithFormat:@"%@ (%@)\nQuestion Score (%@)",
                                   [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"user.name"],
                                   [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"user.score"],
                                   [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKeyPath:@"score"]];
    }

    [cell.detailTextLabel setText:detailsText];

    NSString * question = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"question"];

    [cell setQuestionText:question];

    BOOL lock = [[[self.loadedData objectAtIndex:(NSUInteger) indexPath.row] valueForKey:@"closed"] boolValue];
    [cell lockQuestion:lock];

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * question = [[self.loadedData objectAtIndex:(NSUInteger) indexPath.row ] valueForKeyPath:@"question"];
    CGSize maximumQuestionLabelSize = CGSizeMake(questionTable.frame.size.width - 14, FLT_MAX);
    float expectedQuestionLabelSize = [SizeUtil text:question heightWithFont:[UIFont systemFontOfSize:14] constrainedToSize:maximumQuestionLabelSize];

    return expectedQuestionLabelSize + 63;

}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString * id;

    id = [[self.loadedData objectAtIndex:(NSUInteger) (indexPath.row)] valueForKeyPath:@"id"];

    if([delegate respondsToSelector:@selector(detailsListItemSelected:)]){
        [delegate detailsListItemSelected:id];
    }

}



- (void)dealloc {
    [super dealloc];
}


@end