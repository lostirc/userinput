//
// Created by frank on 11/21/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "ActionSheetHelper.h"

static NSString * shareDidClose = @"shareDidClose";
@implementation ActionSheetHelper {

}

@synthesize context;
-(void) openActionSheetWithTitle:(const uint8_t *)title withTitleLen:(uint32_t)titleLen {

    UIActionSheet * actionSheet = [[[UIActionSheet alloc] initWithTitle:[self uintToNSString:title withLen:titleLen] delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Post to Facebook", @"Send to Friend", @"Copy to Clipboard", nil] autorelease];
    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [actionSheet showInView:[rootController view]];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    NSString * shareClosedEvent = [NSString stringWithFormat:@"{\"buttonIndex\":\"%d\"}", buttonIndex];
    FREDispatchStatusEventAsync(context, (uint8_t *)[shareDidClose UTF8String], (uint8_t*) [shareClosedEvent UTF8String]);
}

-(NSString *) uintToNSString:(const uint8_t *) str
                     withLen:(uint32_t) len{
    return [[[NSString alloc] initWithBytes:str length:len encoding:NSUTF8StringEncoding] autorelease];

}

@end