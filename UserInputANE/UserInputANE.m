#import "FlashRuntimeExtensions.h"
#import "UserInput.h"
#import "ProgressHelper.h"
#import "ActionSheetHelper.h"
#import "FacebookHelper.h"

UserInput * userInput;
ProgressHelper * progressHelper;
ActionSheetHelper * actionSheetHelper;

//share
FREObject ShareToOtherUserWall(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * data;
    uint32_t dataLen;
    FREGetObjectAsUTF8(argv[0], &dataLen, &data);

    const uint8_t * toUserId;
    uint32_t toUserIdLen;
    FREGetObjectAsUTF8(argv[1], &toUserIdLen, &toUserId);

    [FacebookHelper shareToFacebookWall:data withDataLen:dataLen withOtherUser:toUserId withOtherUserLen:toUserIdLen];
    return NULL;
}

FREObject ShareToCurrentUserWall(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * data;
    uint32_t dataLen;
    FREGetObjectAsUTF8(argv[0], &dataLen, &data);
    [FacebookHelper shareToFacebookWall:data withDataLen:dataLen];
    return NULL;
}


//action sheet
FREObject OpenActionSheet(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * title;
    uint32_t titleLen;
    FREGetObjectAsUTF8(argv[0], &titleLen, &title);

    [actionSheetHelper openActionSheetWithTitle:title withTitleLen:titleLen];
    return NULL;
}

//activity popup
FREObject OpenActivityPopup(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{

    [progressHelper openActivityPopup];
    return NULL;
}

FREObject CloseActivityPopup(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{

    [progressHelper closeActivityPopup];
    return NULL;
}



//details view
FREObject PopulateTopView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * data;
    uint32_t dataLen;
    FREGetObjectAsUTF8(argv[0], &dataLen, &data);
    [userInput populateTopData:data withDataLen:dataLen];
    return NULL;
}

FREObject OpenTopView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput openTopView];
    return NULL;
}

FREObject PopulateShareView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * data;
    uint32_t dataLen;
    FREGetObjectAsUTF8(argv[0], &dataLen, &data);
    [userInput populateShareView:data withDataLen:dataLen];
    return NULL;
}

FREObject PopulateInboxView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * data;
    uint32_t dataLen;
    FREGetObjectAsUTF8(argv[0], &dataLen, &data);
    [userInput populateInboxData:data withDataLen:dataLen];
    return NULL;
}


FREObject OpenInboxView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput openInboxView];
    return NULL;
}


FREObject OpenUserDetailsView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput openUserDetailsView];
    return NULL;
}

FREObject PopulateUserDetailsView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * data;
    uint32_t dataLen;
    FREGetObjectAsUTF8(argv[0], &dataLen, &data);
    [userInput populateUserDetailsView:data
                       withDataLen:dataLen];

    return NULL;
}

FREObject OpenDetailsView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput openDetailsView];
    return NULL;
}
FREObject PopulateDetailsView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    const uint8_t * data;
    uint32_t dataLen;
    FREGetObjectAsUTF8(argv[0], &dataLen, &data);
    [userInput populateDetailsView:data
                       withDataLen:dataLen];

    return NULL;
}

FREObject SwitchView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]){
    int32_t value;
    FREGetObjectAsInt32(argv[0], &value);
    [userInput switchView:value];

    return NULL;
}

FREObject CloseDetailsView(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput closeDetailsView];
    return NULL;
}



//bottom text input
FREObject OpenBottomTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput openBottomTextInput];
    return NULL;
}

FREObject CloseBottomTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput closeBottomTextInput];
    return NULL;
}

FREObject CreateBottomTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput createBottomTextInput];
    return NULL;
}

FREObject RemoveBottomTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput removeBottomTextInput];
    return NULL;
}

//top text input
FREObject OpenTopTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput openTopTextInput];
    return NULL;
}

FREObject CloseTopTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput closeTopTextInput];
    return NULL;
}

FREObject CreateTopTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput createTopTextInput];
    return NULL;
}

FREObject RemoveTopTextInput(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    [userInput removeTopTextInput];
    return NULL;
}

//slide share dialog
FREObject OpenShareDialog(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[]){


    [userInput openShareDialog];
    return NULL;

}


FREObject init(FREContext ctx, void* funcData, uint32_t argc, FREObject argv[])
{
    //create a new helper
    if(!userInput){
        userInput = [[UserInput alloc] init];
        [userInput setContext:ctx];
    }
    if(!progressHelper){
        progressHelper = [[ProgressHelper alloc] init];
        [progressHelper setContext:ctx];
    }
    if(!actionSheetHelper){
        actionSheetHelper = [[ActionSheetHelper alloc] init];
        [actionSheetHelper setContext:ctx];
    }

    return NULL;
}


//ane context
void ContextInitializer(void* extData, const uint8_t* ctxType, FREContext ctx, uint32_t* numFunctions, const FRENamedFunction** functions)
{
    *numFunctions = 26;
    FRENamedFunction* func = (FRENamedFunction*)malloc(sizeof(FRENamedFunction)*(*numFunctions));

    func[0].name = (const uint8_t*)"init";
    func[0].functionData = NULL;
    func[0].function = &init;


    func[1].name = (const uint8_t*)"openBottomTextInput";
    func[1].functionData = NULL;
    func[1].function = &OpenBottomTextInput;

    func[2].name = (const uint8_t*)"closeBottomTextInput";
    func[2].functionData = NULL;
    func[2].function = &CloseBottomTextInput;

    func[3].name = (const uint8_t*)"openShareDialog";
    func[3].functionData = NULL;
    func[3].function = &OpenShareDialog;

    func[4].name = (const uint8_t*)"removeBottomTextInput";
    func[4].functionData = NULL;
    func[4].function = &RemoveBottomTextInput;

    func[5].name = (const uint8_t*)"createBottomTextInput";
    func[5].functionData = NULL;
    func[5].function = &CreateBottomTextInput;

    func[6].name = (const uint8_t*)"createTopTextInput";
    func[6].functionData = NULL;
    func[6].function = &CreateTopTextInput;

    func[7].name = (const uint8_t*)"openTopTextInput";
    func[7].functionData = NULL;
    func[7].function = &OpenTopTextInput;

    func[8].name = (const uint8_t*)"closeTopTextInput";
    func[8].functionData = NULL;
    func[8].function = &CloseTopTextInput;

    func[9].name = (const uint8_t*)"removeTopTextInput";
    func[9].functionData = NULL;
    func[9].function = &RemoveTopTextInput;

    func[10].name = (const uint8_t*)"openDetailsView";
    func[10].functionData = NULL;
    func[10].function = &OpenDetailsView;

    func[11].name = (const uint8_t*)"closeDetailsView";
    func[11].functionData = NULL;
    func[11].function = &CloseDetailsView;

    func[12].name = (const uint8_t*)"populateDetailsView";
    func[12].functionData = NULL;
    func[12].function = &PopulateDetailsView;

    func[13].name = (const uint8_t*)"openActivityPopup";
    func[13].functionData = NULL;
    func[13].function = &OpenActivityPopup;

    func[14].name = (const uint8_t*)"closeActivityPopup";
    func[14].functionData = NULL;
    func[14].function = &CloseActivityPopup;

    func[15].name = (const uint8_t*)"openUserDetailsView";
    func[15].functionData = NULL;
    func[15].function = &OpenUserDetailsView;

    func[16].name = (const uint8_t*)"populateUserDetailsView";
    func[16].functionData = NULL;
    func[16].function = &PopulateUserDetailsView;

    func[17].name = (const uint8_t*)"switchView";
    func[17].functionData = NULL;
    func[17].function = &SwitchView;

    func[18].name = (const uint8_t*)"shareToOtherUserWall";
    func[18].functionData = NULL;
    func[18].function = &ShareToOtherUserWall;

    func[19].name = (const uint8_t*)"populateInboxView";
    func[19].functionData = NULL;
    func[19].function = &PopulateInboxView;

    func[20].name = (const uint8_t*)"openInboxView";
    func[20].functionData = NULL;
    func[20].function = &OpenInboxView;

    func[21].name = (const uint8_t*)"openActionSheet";
    func[21].functionData = NULL;
    func[21].function = &OpenActionSheet;

    func[22].name = (const uint8_t*)"populateShareView";
    func[22].functionData = NULL;
    func[22].function = &PopulateShareView;

    func[23].name = (const uint8_t*)"populateTopView";
    func[23].functionData = NULL;
    func[23].function = &PopulateTopView;

    func[24].name = (const uint8_t*)"openTopView";
    func[24].functionData = NULL;
    func[24].function = &OpenTopView;

    func[25].name = (const uint8_t*)"shareToCurrentUserWall";
    func[25].functionData = NULL;
    func[25].function = &ShareToCurrentUserWall;



    *functions = func;

}


void ContextFinalizer(FREContext ctx)
{
    if(userInput){
        [userInput release];
    }

    if(progressHelper){
        [progressHelper release];
    }

    if(actionSheetHelper){
        [actionSheetHelper release];
    }
    return;
}


void UserInputExtInitializer(void** extData, FREContextInitializer* ctxInitializer, FREContextFinalizer* ctxFinalizer)
{
    *extData = NULL;
    *ctxInitializer = &ContextInitializer;
    *ctxFinalizer   = &ContextFinalizer;
}

void UserInputFinalizer(void* extData)
{
    return;
}
