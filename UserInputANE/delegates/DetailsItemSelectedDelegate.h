//
// Created by frank on 11/18/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>

@protocol DetailsItemSelectedDelegate <NSObject>
@optional
    -(void) detailsListItemSelected:(NSString *) item;
    -(void) detailsUserItemSelected:(NSString *) item;
    -(void) detailsMultiValueItemSelected:(NSDictionary *) item;
@end