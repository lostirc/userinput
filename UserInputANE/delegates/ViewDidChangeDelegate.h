//
// Created by frank on 3/19/14.
// Copyright (c) 2014 Eyecu. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol ViewDidChangeDelegate <NSObject>
@optional
-(void) viewDidChange:(NSString *) view;
@end