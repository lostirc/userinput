#import <Foundation/Foundation.h>
#import <MediaPlayer/MPMediaPickerController.h>
#import <UIKit/UIImagePickerController.h>
#import "FlashRuntimeExtensions.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <CoreMedia/CoreMedia.h>
#import <ImageIO/ImageIO.h>
#import <QuartzCore/QuartzCore.h>
#import "views/BottomTextInput.h"
#import "views/TopTextInput.h"
#import "views/DetailsView.h"
#import "QuestionAnswerDetailsView.h"
#import "FavoritesView.h"
#import "ShareView.h"
#import "InboxView.h"
#import "ViewDidChangeDelegate.h"

@interface UserInput : NSObject<QuestionAnswerDetailsViewDelegate, SharedQuestionDeletedDelegate, DetailsViewDelegate, UserDetailsViewDelegate,
        UINavigationControllerDelegate, UIGestureRecognizerDelegate, BottomTextInputSubmittedDelegate, TopTextInputSubmittedDelegate, ShareWithFriendDelegate, ViewDidChangeDelegate> {
}


@property FREContext context;


-(void) openShareDialog;

-(void) openBottomTextInput;
-(void) closeBottomTextInput;
-(void) createBottomTextInput;
-(void) removeBottomTextInput;

-(void) openTopTextInput;
-(void) closeTopTextInput;
-(void) createTopTextInput;
-(void) removeTopTextInput;

-(void) openDetailsView;
-(void) openUserDetailsView;

- (void)openInboxView;

- (void)openTopView;

-(void) populateUserDetailsView:(const uint8_t *) withData
                withDataLen:(uint32_t) dataLen;
-(void) populateDetailsView:(const uint8_t *) withData
                withDataLen:(uint32_t) dataLen;
-(void) populateShareView:(const uint8_t *) withData
                withDataLen:(uint32_t) dataLen;


- (void)populateInboxData:(const uint8_t *)withData withDataLen:(uint32_t)dataLen;

- (void)populateTopData:(const uint8_t *)withData withDataLen:(uint32_t)dataLen;

-(void) switchView:(int32_t) value;

-(void) closeDetailsView;


@property(nonatomic, assign)id  delegate;

@end