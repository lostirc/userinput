//
// Created by frank on 11/22/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SizeUtil : NSObject
+(float)text:(NSString*)text heightWithFont:(UIFont*)font constrainedToSize:(CGSize)size;
@end