//
// Created by frank on 11/22/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "SizeUtil.h"


@implementation SizeUtil {

}

+(float)text:(NSString*)text heightWithFont:(UIFont*)font constrainedToSize:(CGSize)size{

    NSDictionary *attributesDictionary = [NSDictionary dictionaryWithObjectsAndKeys:
            font, NSFontAttributeName,
            nil];

    CGRect frame = [text boundingRectWithSize:size
                                      options:(NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading)
                                   attributes:attributesDictionary
                                      context:nil];

    return ceilf(frame.size.height);
}
@end