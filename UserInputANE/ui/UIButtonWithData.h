//
// Created by frank on 11/12/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIButtonWithData : UIButton

@property (nonatomic, strong) NSString * data;

@end