//
// Created by frank on 10/21/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "DetailsTableViewCell.h"
#import "SizeUtil.h"

@interface DetailsTableViewCell()
    @property(nonatomic, retain) UIImageView * lockIconView;
@end

@implementation DetailsTableViewCell{

}

@synthesize imageView = _imageView;


@synthesize questionLabel = _questionLabel;


@synthesize lockIconView = _lockIconView;


-(CGFloat) questionLabelFontSize{
    return 14;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reuseIdentifier];
    if (self) {

        [self setBackgroundColor:[UIColor clearColor]];
        [self setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setIndentationWidth:0.0];

        NSString * backgroundLoc = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"viewBackground"]
                                                                   ofType:@".png"
                                                              inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

        UIImage * backgroundImage = [[UIImage imageWithContentsOfFile:backgroundLoc] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                                    resizingMode:UIImageResizingModeTile];



        UIView * backgroundImageFill = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, 54)] autorelease];
        [backgroundImageFill setTag:1];
        [backgroundImageFill.layer setBorderColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f].CGColor];
        [backgroundImageFill.layer setBorderWidth:1];
        [backgroundImageFill.layer setCornerRadius:2];
        [backgroundImageFill setClipsToBounds:YES];
        [backgroundImageFill setBackgroundColor:[UIColor colorWithPatternImage:backgroundImage]];

        [self.contentView addSubview:backgroundImageFill];

        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 50, 50)];
        [_imageView.layer setCornerRadius:2];
        [_imageView setClipsToBounds:YES];
        [self.contentView addSubview:_imageView];

        _questionLabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 54, self.frame.size.width - 4, 0)];
        [_questionLabel setTextAlignment:NSTextAlignmentCenter];
        [_questionLabel setLineBreakMode:NSLineBreakByWordWrapping];
        [_questionLabel setFont:[UIFont systemFontOfSize:[self questionLabelFontSize]]];
        [_questionLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
        [_questionLabel setNumberOfLines:0];
        [self.contentView addSubview:_questionLabel];



        NSString * lockIcon = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"lock"]
                                                                      ofType:@".png"
                                                                 inDirectory:@"com/eyecu/icebreaker/mobile/assets"];

        UIImage * lockImage = [[UIImage imageWithContentsOfFile:lockIcon] resizableImageWithCapInsets:UIEdgeInsetsZero
                                                                                         resizingMode:UIImageResizingModeStretch];

        _lockIconView = [[[UIImageView alloc] initWithImage:lockImage] autorelease];
        [_lockIconView setFrame:CGRectMake(52, 37, 12, 15)];
        [_lockIconView setHidden:YES];
        [self.contentView addSubview:_lockIconView];
    }

    return self;
}


-(void) setQuestionText:(NSString *) question{
    [_questionLabel setText:question];

    CGSize maximumQuestionLabelSize = CGSizeMake(_questionLabel.frame.size.width, FLT_MAX);
    CGRect newViewFrame = _questionLabel.frame;
    newViewFrame.size.height = [SizeUtil text:question heightWithFont:_questionLabel.font constrainedToSize:maximumQuestionLabelSize];
    [_questionLabel setFrame:newViewFrame];

    newViewFrame.size.height = _questionLabel.frame.origin.y + _questionLabel.frame.size.height + 6;
    [self setFrame:newViewFrame];

    UIView * background = [self.contentView viewWithTag:1];
    [background setFrame:CGRectMake(0, 0, _questionLabel.frame.size.width + 4, _questionLabel.frame.size.height + 60)];

}

- (void)setFrame:(CGRect)frame {
    frame.origin.x += 5;
    frame.size.width -= 10;

    [super setFrame:frame];
}


- (void)layoutSubviews {
    [super layoutSubviews];
    self.imageView.frame = CGRectMake(2, 2, 50, 50);
    [self.imageView.layer setCornerRadius:2];
    [self.imageView setClipsToBounds:YES];


    [self.detailTextLabel setFrame:CGRectMake(54, 0, self.frame.size.width - 56, 50)];
    [self.detailTextLabel setTextAlignment:NSTextAlignmentCenter];
    [self.detailTextLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [self.detailTextLabel setFont:[UIFont boldSystemFontOfSize:17.0]];
    [self.detailTextLabel setTextColor:[UIColor colorWithRed:51/255.0f green:51/255.0f blue:51/255.0f alpha:1.0f]];
    [self.detailTextLabel setNumberOfLines:0];

}


- (void)prepareForReuse {
    [super prepareForReuse];
    [_imageView setImage:nil];
}


- (void)lockQuestion:(BOOL) lock{
    if(lock){
        [_lockIconView setHidden:NO];
    } else {
        [_lockIconView setHidden:YES];
    }
}

- (void)dealloc {
    [_imageView release];
    [_questionLabel release];
    [_lockIconView release];
    [super dealloc];
}


@end