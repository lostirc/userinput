//
// Created by frank on 10/21/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DetailsTableViewCell : UITableViewCell



@property (nonatomic, strong) UIImageView * imageView;

@property (nonatomic, strong) UILabel * questionLabel;

- (CGFloat)questionLabelFontSize;

- (void)setQuestionText:(NSString *)question;

- (void)lockQuestion:(BOOL)lock;
@end