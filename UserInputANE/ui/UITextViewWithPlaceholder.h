
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface UITextViewWithPlaceholder : UITextView

    @property (nonatomic, retain) NSString *placeholder;


    -(void)textChanged:(NSNotification*)notification;

@end