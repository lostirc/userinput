
#import "UserInput.h"


static NSString * bottomTextInputSubmit = @"bottomTextInputSubmit";
static NSString * topTextInputSubmit = @"topTextInputSubmit";
static NSString * topTextInputClosed = @"topTextInputClosed";
static NSString * detailsViewDataRequest = @"detailsViewDataRequest";
static NSString * detailsViewClosed = @"detailsViewClosed";
static NSString * detailsViewClosing = @"detailsViewClosing";
static NSString * detailsItemSelected = @"detailsItemSelected";
static NSString * detailsMultiItemSelected = @"detailsMultiItemSelected";
static NSString * userItemSelected = @"userItemSelected";
static NSString * shareCurrentQuestionWithFriend = @"shareCurrentQuestionWithFriend";
static NSString * sharedQuestionDeleted = @"sharedQuestionDeleted";
static NSString * viewDiDChange = @"viewDidChange";

@implementation UserInput{

    BottomTextInput * bottomTextInput;
    TopTextInput * topTextInput;
    DetailsView * detailsView;

}


@synthesize context;

//text input view
-(void) openShareDialog {


    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];

    CGFloat viewHeight = rootController.view.frame.size.height - 104;

    detailsView = [[DetailsView new] withSuperview:[rootController view] withView:kShareView];
    [detailsView setFrame:CGRectMake(0, rootController.view.frame.size.height, rootController.view.frame.size.width, viewHeight)];
    detailsView.delegate = self;

    [detailsView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [rootController.view addSubview:detailsView];
    [self animateDetailsViewUp:kShareView];
}

//details view

-(void) openDetailsView{

    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];

    CGFloat viewHeight = rootController.view.frame.size.height - 104;

    detailsView = [[DetailsView new] withSuperview:[rootController view] withView:kDetailView];
    [detailsView setFrame:CGRectMake(0, rootController.view.frame.size.height, rootController.view.frame.size.width, viewHeight)];
    detailsView.delegate = self;

    [detailsView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [rootController.view addSubview:detailsView];
    [self animateDetailsViewUp:kDetailView];


}

-(void) openUserDetailsView{

    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];

    CGFloat viewHeight = rootController.view.frame.size.height - 104;

    detailsView = [[DetailsView new] withSuperview:[rootController view] withView:kUserView];
    [detailsView setFrame:CGRectMake(0, rootController.view.frame.size.height, rootController.view.frame.size.width, viewHeight)];
    detailsView.delegate = self;

    [detailsView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [rootController.view addSubview:detailsView];
    [self animateDetailsViewUp:kUserView];
}

-(void) openInboxView{

    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];

    CGFloat viewHeight = rootController.view.frame.size.height - 104;

    detailsView = [[DetailsView new] withSuperview:[rootController view] withView:kInboxView];
    [detailsView setFrame:CGRectMake(0, rootController.view.frame.size.height, rootController.view.frame.size.width, viewHeight)];
    detailsView.delegate = self;

    [detailsView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [rootController.view addSubview:detailsView];
    [self animateDetailsViewUp:kInboxView];
}



-(void) openTopView{

    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];

    CGFloat viewHeight = rootController.view.frame.size.height - 104;

    detailsView = [[DetailsView new] withSuperview:[rootController view] withView:kTopView];
    [detailsView setFrame:CGRectMake(0, rootController.view.frame.size.height, rootController.view.frame.size.width, viewHeight)];
    detailsView.delegate = self;

    [detailsView setBackgroundColor:[UIColor colorWithRed:46/255.0f green:146/255.0f blue:206/255.0f alpha:1.0f]];
    [rootController.view addSubview:detailsView];
    [self animateDetailsViewUp:kTopView];

}

-(void) animateDetailsViewUp: (ViewOpenType) view {

    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    CGFloat viewHeight = rootController.view.frame.size.height - 104;


    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = detailsView.frame;
        frame.origin.y = rootController.view.frame.size.height - viewHeight;
        detailsView.frame = frame;
    } completion:^(BOOL finished){
        if(view != kReopenSameView){
            [detailsView checkIfDataLoadNeeded:view];
        }
    }];
}

-(void) dispatchDetailsDataRequest: (ViewOpenType) viewOpenType {
    NSString * view = @"";
    switch (viewOpenType){
        case kUserView:
            view = @"user";
            break;
        case kDetailView:
            view = @"details";
            break;
        case kInboxView:
            view = @"inbox";
            break;
        case kShareView:
            view = @"share";
            break;
        case kTopView:
            view = @"top";
            break;
        case kReopenSameView:break;
    }
    NSString * detailsOpenedEvent = [NSString stringWithFormat:@"{\"requestedData\":\"%@\"}", view];
    FREDispatchStatusEventAsync(context, (uint8_t *)[detailsViewDataRequest UTF8String], (uint8_t*) [detailsOpenedEvent UTF8String]);
}

-(void)closeDetailsView {
    NSString * detailsClosingEvent = [NSString stringWithFormat:@"{\"details\":\"closing\"}"];
    FREDispatchStatusEventAsync(context, (uint8_t *)[detailsViewClosing UTF8String], (uint8_t*) [detailsClosingEvent UTF8String]);

    UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    [UIView animateWithDuration:0.3 animations:^{
        CGRect frame = detailsView.frame;
        frame.origin.y = rootController.view.frame.size.height;
        detailsView.frame = frame;
    } completion:^(BOOL finished) {
        [detailsView release];
        NSString * detailsClosedEvent = [NSString stringWithFormat:@"{\"details\":\"closed\"}"];
        FREDispatchStatusEventAsync(context, (uint8_t *)[detailsViewClosed UTF8String], (uint8_t*) [detailsClosedEvent UTF8String]);
    }];

}


- (void)detailsViewDragEnded:(CGPoint)location {
    NSLog(@"drag ended");
    if(location.y < 150){
        [self animateDetailsViewUp:kReopenSameView];
    } else {
        [self closeDetailsView];
    }
}

-(void) switchView:(int32_t) value{
    NSLog(@"value is: %i", value);
    switch (value){
        case kDetailView:
            [detailsView switchView:kDetailView];
            break;
        case kUserView:
            [detailsView switchView:kUserView];
            break;
        case kInboxView:
            [detailsView switchView:kInboxView];
            break;
        case kTopView:
            [detailsView switchView:kTopView];
            break;
        default:break;
    }
}

-(void) populateShareView:(const uint8_t *) withData withDataLen:(uint32_t) dataLen{
    NSString * jsonData = [self uintToNSString:withData withLen:dataLen];
    [detailsView setShareData:jsonData responder:self];
}

-(void) populateDetailsView:(const uint8_t *) withData withDataLen:(uint32_t) dataLen{
    NSString * jsonData = [self uintToNSString:withData withLen:dataLen];
    [detailsView setDetailsData:jsonData responder:self];
}

-(void) populateUserDetailsView:(const uint8_t *) withData withDataLen:(uint32_t) dataLen{
    NSString * jsonData = [self uintToNSString:withData withLen:dataLen];
    [detailsView setUserDetailsData:jsonData responder:self];
}


-(void) populateInboxData:(const uint8_t *) withData withDataLen:(uint32_t) dataLen{
    NSString * jsonData = [self uintToNSString:withData withLen:dataLen];
    [detailsView setInboxViewData:jsonData responder:self];
}

-(void) populateTopData:(const uint8_t *) withData withDataLen:(uint32_t) dataLen{
    NSString * jsonData = [self uintToNSString:withData withLen:dataLen];
    [detailsView setTopViewData:jsonData responder:self];
}

- (void)detailsItemSelected:(NSString *)item {
    NSString * detailsItem = [NSString stringWithFormat:@"{\"text\":\"%@\"}", item];
    FREDispatchStatusEventAsync(context, (uint8_t *)[detailsItemSelected UTF8String], (uint8_t*) [detailsItem UTF8String]);
    [self closeDetailsView];
}

- (void)detailsMultiItemSelected:(NSDictionary *)item {
    NSError * e;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:item options:NSJSONWritingPrettyPrinted error:&e];
    NSString * jsonString = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
    FREDispatchStatusEventAsync(context, (uint8_t *)[detailsMultiItemSelected UTF8String], (uint8_t*) [jsonString UTF8String]);
    [self closeDetailsView];
}


- (void)userItemSelected:(NSString *)item {
    FREDispatchStatusEventAsync(context, (uint8_t *)[userItemSelected UTF8String], (uint8_t*) [item UTF8String]);
    [self closeDetailsView];
}

- (void)sharedQuestionDeleted:(NSString *)item {
    NSString * detailsItem = [NSString stringWithFormat:@"{\"text\":\"%@\"}", item];
    FREDispatchStatusEventAsync(context, (uint8_t *)[sharedQuestionDeleted UTF8String], (uint8_t*) [detailsItem UTF8String]);
}


- (void)shareWithFriends:(SelectedFriend *)selectedFriend {
    NSString * friend = [NSString stringWithFormat:@"{\"friendId\":\"%@\",\"friendType\":\"%d\"}", selectedFriend.friendId, selectedFriend.friendType];
    FREDispatchStatusEventAsync(context, (uint8_t *)[shareCurrentQuestionWithFriend UTF8String], (uint8_t*) [friend UTF8String]);
    [self closeDetailsView];
}

-(void)viewDidChange:(NSString *) view{
    NSLog(@"View Did Change, %@", view);
    NSString * viewChange = [NSString stringWithFormat:@"{\"text\":\"%@\"}", view];
    FREDispatchStatusEventAsync(context, (uint8_t *)[viewDiDChange UTF8String], (uint8_t*) [viewChange UTF8String]);
}

//bottom text input
-(void) createBottomTextInput{
    NSLog(@"Add bottom text input");
    UIViewController *rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    bottomTextInput = [[BottomTextInput alloc] init];
    bottomTextInput.delegate = self;

    [bottomTextInput allocBottomTextInput:[rootController view]];
}

-(void) removeBottomTextInput{
    [bottomTextInput release];
}

-(void) openBottomTextInput{
    [bottomTextInput openBottomTextInput];
}

-(void) closeBottomTextInput{

    [bottomTextInput closeBottomTextInput];

}

- (void)bottomTextSubmitted:(NSString *)text {
    text = [[NSString stringWithFormat:@"%@", text] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString * textSubmittedEvent = [NSString stringWithFormat:@"{\"text\":\"%@\",\"submitted\":\"true\"}", text];
    FREDispatchStatusEventAsync(context, (uint8_t *)[bottomTextInputSubmit UTF8String], (uint8_t*) [textSubmittedEvent UTF8String]);
    NSLog(@"Bottom text submitted: %@",text);
}


//top text input

-(void) openTopTextInput{
    NSLog(@"UserInput: openTopTextInput");
    [topTextInput openTopTextInput];

}
-(void) closeTopTextInput{
    NSLog(@"UserInput: closeTopTextInput");
    [topTextInput closeTopTextInput];
}

- (void)topTextClosed {
    NSString * textSubmittedEvent = [NSString stringWithFormat:@"{\"text\":\"\",\"submitted\":\"false\"}"];
    FREDispatchStatusEventAsync(context, (uint8_t *)[topTextInputClosed UTF8String], (uint8_t*) [textSubmittedEvent UTF8String]);
}


-(void) createTopTextInput{

    UIViewController *rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
    topTextInput = [[TopTextInput alloc] init];
    topTextInput.delegate = self;
    [topTextInput allocTopTextInput:[rootController view]];


}

-(void) removeTopTextInput{
    [topTextInput release];

}

- (void)topTextSubmitted:(NSString *)text {
    text = [[NSString stringWithFormat:@"%@", text] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    NSString * textSubmittedEvent = [NSString stringWithFormat:@"{\"text\":\"%@\",\"submitted\":\"true\"}", text];
    FREDispatchStatusEventAsync(context, (uint8_t *)[topTextInputSubmit UTF8String], (uint8_t*) [textSubmittedEvent UTF8String]);
    NSLog(@"Top text submitted: %@",text);
    [self closeTopTextInput];
}




-(NSString *) uintToNSString:(const uint8_t *) str
                    withLen:(uint32_t) len{
    return [[[NSString alloc] initWithBytes:str length:len encoding:NSUTF8StringEncoding] autorelease];

}

- (void)dealloc {
    if(topTextInput){
        [topTextInput release];
    }
    if(bottomTextInput){
        [bottomTextInput release];
    }
    if(detailsView){
        [detailsView release];
    }
    [super dealloc];
}


@end