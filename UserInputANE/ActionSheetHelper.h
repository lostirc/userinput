//
// Created by frank on 11/21/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FlashRuntimeExtensions.h"


@interface ActionSheetHelper : NSObject <UIActionSheetDelegate>

@property FREContext context;
-(void) openActionSheetWithTitle:(const uint8_t *)title withTitleLen:(uint32_t)titleLen;
@end