//
// Created by frank on 11/5/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "FlashRuntimeExtensions.h"
#import <UIKit/UIKit.h>
#import "MDProgressHUD/MBProgressHUD.h"


@interface ProgressHelper : NSObject

@property FREContext context;

-(void) openActivityPopup;
-(void) closeActivityPopup;

@end