//
// Created by frank on 11/5/13.
// Copyright (c) 2013 Eyecu. All rights reserved.
//


#import "ProgressHelper.h"


@implementation ProgressHelper {
    UIView * modalView;
    bool popupIsOpen;
}

@synthesize context;

- (void)openActivityPopup {
    if(!popupIsOpen){
        UIViewController * rootController = [[[UIApplication sharedApplication] keyWindow] rootViewController];
        CGRect rootFrame = rootController.view.frame;
        modalView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rootFrame.size.width, rootFrame.size.height)];

        [rootController.view addSubview:modalView];
        [MBProgressHUD showHUDAddedTo:modalView animated:YES];

        popupIsOpen = true;
    }
}

- (void)closeActivityPopup {
    if(popupIsOpen){
        [MBProgressHUD hideHUDForView:modalView animated:YES];
        [modalView removeFromSuperview];
        [modalView release];
        popupIsOpen = false;
    }

}


- (void)dealloc {
    [modalView release];
    [super dealloc];
}

@end